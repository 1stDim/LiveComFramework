"""
Conclusion:
    When deployed on production, the thread "abuse" won't affect the performance.
"""

import threading
import gc
from time import sleep

def outer_func(val):
    for i in range(val):
        inner_func(i)

def inner_func(counter):
    t = threading.Thread(target=dummy_worker, name="Count=" + str(counter))
    t.start()
    #t.join()
    pass

def dummy_worker():
    #print("dummy worker! now will exit.")
    a = 1
    sleep(0.1)
    return

print("Collect init: " + str(gc.collect()))
print("100 threads")
outer_func(100)
print("Collect 100 threads: " + str(gc.collect()))

print("100 threads")
outer_func(100)
print("Collect 100 threads: " + str(gc.collect()))

print("100 threads")
outer_func(100)
print("Collect 100 threads: " + str(gc.collect()))

print("1000 threads")
outer_func(1000)
print("Collect 1000 threads: " + str(gc.collect()))

print("1000 threads")
outer_func(1000)
print("Collect 1000 threads: " + str(gc.collect()))

print("1000 threads")
outer_func(1000)
print("Collect 1000 threads: " + str(gc.collect()))

print("10000 threads")
outer_func(10000)
print("Collect 10000 threads: " + str(gc.collect()))

print("10000 threads")
outer_func(10000)
print("Collect 10000 threads: " + str(gc.collect()))

print("10000 threads")
outer_func(10000)
print("Collect 10000 threads: " + str(gc.collect()))

print("100000 threads")
outer_func(100000)
print("Collect 100000 threads: " + str(gc.collect()))

print("100000 threads")
outer_func(100000)
print("Collect 100000 threads: " + str(gc.collect()))

print("100000 threads")
outer_func(100000)
print("Collect 100000 threads: " + str(gc.collect()))

print("100 threads")
outer_func(100)
print("Collect 100 threads: " + str(gc.collect()))

print("100 threads")
outer_func(100)
print("Collect 100 threads: " + str(gc.collect()))

print("100 threads")
outer_func(100)
print("Collect 100 threads: " + str(gc.collect()))