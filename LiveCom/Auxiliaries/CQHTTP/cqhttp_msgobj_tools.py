def msgobj_generator(context, send_target_only=False):
    srcname="CQHTTP:"
    if context["message_type"] == "private":
        # private message
        srcname += "@"

        try:
            if context["sub_type"] == "friend":
                srcname += "F:"
            elif context["sub_type"] == "group":
                srcname += "G:"
            elif context["sub_type"] == "discuss":
                srcname += "D:"
            elif context["sub_type"] == "other":
                srcname += "O:"
        except KeyError as e:
            if e.args[0] == "sub_type":
                srcname += "O:"
            else:
                raise e

        srcname += "U" + str(context["user_id"])

    elif context["message_type"] == "group":
        # group message
        srcname += "G" + str(context["group_id"])
        if send_target_only:
            return srcname
        try:
            if context["sub_type"] == "normal":
                try:
                    srcname += ":U" + str(context["user_id"])
                except KeyError as e:
                    if e.args[0] == "user_id":
                        pass
                    else:
                        raise e

            elif context["sub_type"] == "anonymous":
                srcname += ":A\"" + str(context["anonymous"]) + "\""
            elif context["sub_type"] == "notice":
                srcname += ":N"
            else:
                print("CQHTTP: Error: Unknown sub_type=" + str(context["sub_type"]))
                srcname += "-UNKNOWNSUBTYPE:-NOUSERINFO"
        except KeyError as e:
                if e.args[0] == "sub_type":
                    pass
                else:
                    raise e

    elif context["message_type"] == "discuss":
        srcname += "D" + str(context["discuss_id"])
        if send_target_only:
            return srcname
        try:
            srcname += ":U" + str(context["user_id"])
        except KeyError as e:
            if e.args[0] == "user_id":
                pass
            else:
                raise e
    else:
        print("CQHTTP: Error: Unknown message_type=" + str(context["messageb_type"]))
        srcname += "-UNKNOWNMSGTYPE:-NOUSERINFO"

    return srcname


def msgobj_paraser(context: str):
    # check from CQHTTP
    if not context[0:len("CQHTTP:")] == "CQHTTP:":
        return None # not a target

    answer = {"post_type": "message"}

    # yes this is the wanted type
    slist = context.split(":")

    # slist[0] = CQHTTP
    # slist[1] = container type @?/G*/D*
    # slist[2] = target type (different from container type.

    # prepare partial indicator
    answer["partial"] = False
    # container types:
    if slist[1][0:1] == "@":
        # type P?
        answer["message_type"] = "private"
        if slist[1][1:2] == "F":
            answer["sub_type"] = "friend"
        elif slist[1][1:2] == "G":
            answer["sub_type"] = "group"
        elif slist[1][1:2] == "D":
            answer["sub_type"] = "discuss"
        elif slist[1][1:2] == "O":
            answer["sub_type"] = "other"
        else:
            answer["partial"] = True

        answer["user_id"] = slist[2][1:]
    elif slist[1][0:1] == "G":
        # type G*
        answer["message_type"] = "group"
        answer["group_id"] = slist[1][1:]
        try:
            if slist[2][0:1] == "U":
                answer["sub_type"] = "normal"
                answer["user_id"] = slist[2][1:]
            elif slist[2][0:1] == "A":
                answer["sub_type"] = "anonymous"
                answer["anonymous"] = slist[2][2:-1]
            elif slist[2][0:1] == "N":
                answer["sub_type"] = "notice"
            else:
                raise ValueError("Unknown target value type in CQHTTP:G*")
        except IndexError as e:
            answer["partial"] = True
    elif slist[1][0:1] == "D":
        # type G*
        answer["message_type"] = "discuss"
        answer["discuss_id"] = slist[1][1:]
        try:
            answer["user_id"] = slist[2][1:]
        except IndexError as e:
            answer["partial"] = True
    else:
        raise ValueError("Unknown container value type in CQHTTP")

    return answer


def get_send_target(src):
    if isinstance(src, str):
        return msgobj_generator(msgobj_paraser(src), send_target_only=True)
    elif isinstance(src, dict):
        return msgobj_generator(src, send_target_only=True)
    else:
        raise TypeError("must be str or dict, not " + str(src.__class__.__name__))
