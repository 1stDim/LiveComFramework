# Format utilities.
from typing import List, Dict


class InlineFormatSyntaxError(Exception):
    pass


def pack(src: List[Dict[str, str]]) -> str:
    # read item and construct target inline format
    output = ""
    for item in src:
        if item["type"] == "text":
            # text type: just do a base convert and send as is.
            output += _text_escape(item["text"])
        else:
            base_content = ''
            # other types. construct format as requested.
            # preload keyword "type".
            opdict = dict(item)
            typename = opdict.pop("type")
            base_content += 'type="' + typename + '"'
            # others item use as is
            for key, value in opdict.items():
                out_key = _flush_text(key)
                base_content += ", " + out_key + '="' + \
                                _value_escape(value) \
                                + '"'
            output += "[" + base_content + "]"
            pass
    return output


def unpack(src_text: str) -> List[Dict[str, str]]:
    # first, scan for all bracket
    bracket_info = [(start, end) for start, end in _iter_find_brackets(src_text)]
    # split string into multiple segment
    # step 1 convert bracket_info into (start, len) format with blank filled
    # [
    #     (int(start), int(len), bool(bracket_opt)),
    #     ...
    # ]
    segment_info = []
    front_ptr = 0
    for start, end in bracket_info:
        if start > front_ptr:
            # write last segment first, this is not a bracket.
            segment_info.append((front_ptr, start - front_ptr, False))
        # write current segment.
        segment_info.append((start, end + 1 - start, True))
        # push front_ptr forward
        front_ptr = end + 1

    if front_ptr < len(src_text):
        # here is a remaining segment.
        segment_info.append((front_ptr, len(src_text) - front_ptr, False))

    # segment_info now is ready for use.
    # step 2: use these information to construct output.
    output = []
    for start, lenth, segment_type in segment_info:
        if segment_type:
            # bracket type.
            # extract data direct from segment.
            output.append(_inline_opts_unpack(src_text[start:start+lenth]))
        else:
            # plain text type.
            # construct a text segment.
            output.append(
                {
                    "type": "text",
                    "text": _text_unescape(src_text[start:start+lenth])
                }
            )
    # return result.
    return output


def _test(string: str = "这是一段普通的文字。\[这一段是放在方括号内的。\]这是下一段文字。[type=\"test\"]"):
    pass


def _inline_opts_unpack(source: str) -> Dict[str,str]:
    # # check boundary
    if source[0] != "[" or source[-1] != "]":
        raise InlineFormatSyntaxError("Option unpacking failed due to incorrect entry.",
                                      source, '')
    # extract
    txt = source[1:-1]
    # unpack by column
    tlist = txt.split(",")
    # pre-process options
    for i in range(len(tlist)):
        tlist[i] = tlist[i].strip()

    # unpack by equal mark
    keystore = {}
    for context in tlist:
        try:
            keyname, value = tuple(context.split("=", 1))
        except ValueError:
            raise \
                InlineFormatSyntaxError(
                    "Incorrect argument format in options item processor. Expecting 'key=\"value\"' format.",
                    context
                )
        if value[0] != '"' or value[-1] != '"':
            raise InlineFormatSyntaxError(
                "Incorrect item value format. value is required for quoted. ('key=\"value\"')", context, value
            )
        keystore[keyname] = _value_unescape(value.lstrip('"').rstrip('"'))

    return keystore
    pass

def _iter_find_brackets(source: str):
    # op chars = "\" "[" "]" '"'
    SLASH = "\\"
    LBRACKET = "["
    QUOTE = '"'
    RBRACKET = "]"  # seems useless?

    # init stack object
    stack = []
    # generate op stacks
    for address in range(len(source)):
        if source[address:address + 1] == SLASH:
            stack.append((address, SLASH))
        elif source[address:address + 1] == LBRACKET:
            stack.append((address, LBRACKET))
        elif source[address:address + 1] == RBRACKET:
            stack.append((address, RBRACKET))
        elif source[address:address + 1] == QUOTE:
            stack.append((address, QUOTE))

    d = dict(stack)

    slash_open = False
    quote_open = False
    bracket_open = False
    left_bracket_address = -1
    last_address = -1
    for address in d.keys():
        if last_address + 1 != address:
            slash_open = False

        last_address = address

        if d[address] == SLASH:
            slash_open = not slash_open
            continue
        elif d[address] == LBRACKET:
            if quote_open:
                continue
            if slash_open:
                # cleanup old slash open flag
                slash_open = False
                continue
            if bracket_open:
                raise InlineFormatSyntaxError("Nested left bracket is not allowed.",
                                              source, address)
            # bracket closed: mark open
            bracket_open = True
            left_bracket_address = address
            continue
        elif d[address] == QUOTE:
            if bracket_open and not slash_open:
                quote_open = not quote_open
                continue
            # cleanup old slash open flag
            slash_open = False
            continue  # ignore cases
        elif d[address] == RBRACKET:
            if quote_open:
                continue
            if slash_open:
                # cleanup old slash open flag
                slash_open = False
                continue
            if bracket_open:
                yield (left_bracket_address, address)
                bracket_open = False
                left_bracket_address = -1
                continue
            # something went wrong.
            raise InlineFormatSyntaxError("Missing '[' for the ']'.", source, address)
        else:
            # something went very wrong... but why?
            assert "Inline processor have something very wrong."

    # final check for everything is okay.
    if bracket_open:
        raise InlineFormatSyntaxError("Missing ']' for the '['.", source, len(source))
    if quote_open:
        raise InlineFormatSyntaxError("Missing '"' for pairing '"'.", source, len(source))
    if slash_open:
        raise InlineFormatSyntaxError("Unfinished escape character.", source, len(source))
    # seems everything finished.
    raise StopIteration()


def _flush_text(src: str) -> str:
    # TODO: rewrite here with re library.
    out = str(src)
    for t in list('~!@#$%^&*()+`=[]{};\':"<>,./?|'):
        out = out.replace(t, "_")
    return out


def _value_unescape(src: str) -> str:
    out = str(src)
    escape_rules = [
        ('\\"', '"'),
        ("\\\\", "\\")
    ]
    for f, t in escape_rules:
        out.replace(f, t)
    return out


def _text_unescape(src: str) -> str:
    out = str(src)
    escape_rules = [
        ('\\[', '['),
        ('\\]', ']'),
        ("\\\\", "\\")
    ]
    for f, t in escape_rules:
        out.replace(f, t)
    return out


def _value_escape(src: str) -> str:
    out = str(src)
    escape_rules = [
        ("\\", "\\\\"),
        ('"', '\\"')
    ]
    for f, t in escape_rules:
        out.replace(f, t)
    return out


def _text_escape(src: str) -> str:
    out = str(src)
    escape_rules = [
        ("\\", "\\\\"),
        ('[', '\\['),
        (']', '\\]')
    ]
    for f, t in escape_rules:
        out.replace(f, t)
    return out
