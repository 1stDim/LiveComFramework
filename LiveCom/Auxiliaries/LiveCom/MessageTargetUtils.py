from typing import Optional

def conv_container(target: str) -> str:
    # Example: "CQHTTP:G12345678:U1234567" -> "CQHTTP:G12345678"
    # Example: "CQHTTP:@F:U1234567" -> "CQHTTP:@F:U1234567"
    """
    
    :param str target: 
    :return: 
    :rtype: str
    """
    tlist = target.split(":", maxsplit=3)
    if not (len(tlist) == 2 or len(tlist) == 3):
        raise MalformedMsgTargetNameException(target)

    adaptor_name = tlist[0]
    container_id = tlist[1]
    
    if len(tlist) == 2 and container_id[0] == "@":
        raise MalformedMsgTargetNameException(target)
    
    if len(tlist) == 2:
        return adaptor_name + ":" + container_id
    
    user_id = tlist[2]
    if container_id[0] == "@":
        return adaptor_name + ":" + container_id + ":" + user_id
    else:
        return adaptor_name + ":" + container_id
    
def conv_dataset(target: str) -> tuple:
    """
    
    :param str target: 
    :return: 
    :rtype: str
    """
    tlist = target.split(":", maxsplit=3)
    if not (len(tlist) == 2 or len(tlist) == 3):
        raise MalformedMsgTargetNameException(target)

    adaptor_name = tlist[0]
    container_id = tlist[1]

    if len(tlist) == 2 and container_id[0] == "@":
        raise MalformedMsgTargetNameException(target)

    if len(tlist) == 2:
        return adaptor_name, container_id, None

    user_id = tlist[2]
    return adaptor_name, container_id, user_id
    pass

def extr_adaptor_name(target: str) -> str:
    """

    :param str target:
    :return:
    :rtype: str
    """
    tlist = target.split(":", maxsplit=3)
    if not (len(tlist) == 2 or len(tlist) == 3):
        raise MalformedMsgTargetNameException(target)

    adaptor_name = tlist[0]
    return adaptor_name

def extr_container_id(target: str) -> str:
    """

    :param str target:
    :return:
    :rtype: str
    """
    tlist = target.split(":", maxsplit=3)
    if not (len(tlist) == 2 or len(tlist) == 3):
        raise MalformedMsgTargetNameException(target)

    container_id = tlist[1]
    return container_id
def extr_user_id(target: str) -> Optional[str]:
    """

    :param str target:
    :return:
    :rtype: str|None
    """
    tlist = target.split(":", maxsplit=3)
    if not (len(tlist) == 2 or len(tlist) == 3):
        raise MalformedMsgTargetNameException(target)

    if len(tlist) == 3:
        user_id = tlist[2]
        return user_id

    # Nothing...
    return None

def is_container(target: str, treate_exception_as_false: bool=True):
    tlist = target.split(":", maxsplit=3)
    if not (len(tlist) == 2 or len(tlist) == 3):
        if treate_exception_as_false:
            return False
        else:
            raise MalformedMsgTargetNameException(target)

    container_id = tlist[1]
    if container_id[0] == "@" and len(tlist) == 3:
        return True

    if container_id[0] == "@" and len(tlist) == 2:
        if treate_exception_as_false:
            return False
        else:
            raise MalformedMsgTargetNameException(target)

    if len(tlist) == 2:
        return True

    return False



class MalformedMsgTargetNameException(Exception):
    def __init__(self, name:str):
        self.name = name
    
    def __str__(self):
        return 'Malformed message target name: ' + self.name
