"""
Adaptor Loader
This file used to load every adaptor in Adaptor directory.
"""

# import LiveCom.Adaptors.Frontend.CQ_HTTP_API_PythonSDK as CQHTTP
# import LiveCom.Adaptors.Services.DebugOnly_MsgLoopback as LOOPBACK
#
# def Register(sysbus):
#     CQHTTP.Setup(sysbus)
#     LOOPBACK.Setup(sysbus)

from importlib import import_module, __import__
from gc import collect

# assume every directory has it own __all__ initialize method.

catalogs=[
    "LiveCom.Adaptors.Frontend",
    "LiveCom.Adaptors.Backend",
    "LiveCom.Adaptors.Services"
]


class AdaptorLoadError(Exception):
    pass


def Probe():
    loadmap = {}
    # load placeholder module
    import LiveCom.Auxiliaries.LiveCom.SystemAdaptorPlaceholder as PlaceHolder
    loadmap["LiveComFW"] = PlaceHolder
    for catalog in catalogs:
        mod = import_module(catalog)
        for name in mod.__all__:
            if name not in loadmap.keys():
                loadmap[catalog.split(".")[-1] + "." + name] = import_module(catalog + "." + name)
                print("LiveCom: 初始化: 查找模块: 已找到模块: 模块类型 \""+ catalog.split(".")[-1] + "\": 模块名称 \"" + name + "\"")
            else:
                raise AdaptorLoadError("Conflicted adaptor name in catalog '" + catalog + "': " + name)
    collect()
    return loadmap

# def Register(sysbus):
#     loadmap = Probe()
#     for name in loadmap.keys():
#         loadmap[name].Setup(sysbus)
#         print("LoadeAdaptors: Register: Adaptor \"" + name + "\" registered.")
#
#     collect()