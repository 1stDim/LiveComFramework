from typing import Union, Iterable

def add_routing_group(group_name: str):
    pass

def del_routing_group(group_name: str):
    pass


def set_routing_link(group_name: str, link_name: str, source: str, targets: Union[str, Iterable]):
    pass


def unset_routing_link(group_name: str, link_name: str):
    pass


def get_groups() -> set:
    pass


def get_group_context(group_name: str) -> dict:
    pass