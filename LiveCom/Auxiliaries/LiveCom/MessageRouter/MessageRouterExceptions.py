class DuplicatedRoutingGroupError(Exception):
    def __init__(self, group_name):
        self.group_name = group_name

    def __str__(self):
        return "Duplicated routing group:" + self.group_name

class NoSuchRoutingGroupError(Exception):
    def __init__(self, group_name):
        self.group_name = group_name

    def __str__(self):
        return "No such routing group:" + self.group_name

class NoSuchRoutingLinkError(Exception):
    def __init__(self, group_name, link_name):
        self.group_name = group_name
        self.link_name = link_name

    def __str__(self):
        return "No such routing link in routing group '" + self.group_name + "': " + self.link_name
