from typing import Optional

class NoSuchNameError(Exception):
    def __init__(self, module_codename: Optional[str], module_name: str):
        self.module_name = module_name
        self.module_codename = module_codename

    def __str__(self):
        if self.module_codename is None:
            return "No such module named as '" + self.module_name + \
                   "'registered in NameResolver in all codenames."
        else:
            return "No such module named as '" + self.module_name + \
               "'registered in NameResolver in codename '" + self.module_codename + "'."


class NoSuchCodenameError(Exception):
    def __init__(self, module_codename: str):
        self.module_codename = module_codename

    def __str__(self):
        return "No such codename '" + self.module_codename + "'."
