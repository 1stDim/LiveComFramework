from threading import Lock

class ZeroWaiter(object):
    """
    归零等待

    当有需要等待归零时，调用wait_zero方法。使用add_count和del_count方法操作计数器。
    """
    def __init__(self, init_val: int=0):
        """
        创建归零等待对象

        :param int init_val: 设定初始值。当初始值非零时，调用wait_zero方法将会阻塞。默认值为0。注意：不可指定负数，否则会抛出ValueError异常。
        """
        if init_val < 0:
            raise ValueError("Counter cannot be less than zero.")
        self.counter = init_val
        self._status_lock = Lock()
        if not self.counter == 0:
            self._status_lock.acquire()
        self._aborting = False
        self._op_lock = Lock()

    def wait_zero(self, timeout: int=-1) -> bool:
        """
        等待归零

        :param int timeout: 指定等待超时。
        :return: 是否成功等待到归零。如果本次归零是失败的（主动放弃），或者指定了超时但已超过等待时间，将返回False。如果成功等待到归零，则返回True。
        :rtype: bool
        """
        return_status = self._status_lock.acquire()
        try:
            self._op_lock.acquire()
            if self._aborting:
                    return False
            return return_status
        finally:
            self._op_lock.release()
            if return_status:
                self._status_lock.release()

    def add_count(self) -> int:
        """
        为计数器 +1

        :return: 本次操作后计数器的数值。
        :rtype: int
        """
        # 把上面说明的内容看错成别的不可描述的东西的都给我面壁去！ --SuperMarioSF
        self._op_lock.acquire()
        try:
            self.counter += 1
            if not self._status_lock.locked():
                self._aborting = False
                self._status_lock.acquire()
            return self.counter
        finally:
            self._op_lock.release()

    def del_count(self):
        """
        为计数器 -1

        如果计数器已经归零，调用此方法将会引发ValueError异常，因为计数器不可以是负数。

        :return: 本次操作后计数器的数值。
        :rtype: int
        """
        self._op_lock.acquire()
        try:
            if self.counter <= 0:
                self.counter = 0  # Fix error counts
                raise ValueError("Counter cannot be less than zero.")
            self.counter -= 1
            if self.counter == 0:
                self._status_lock.release()
            return self.counter
        finally:
            self._op_lock.release()

    def abort(self):
        """
        主动放弃本次归零，释放正在通过wait_zero等待的其他线程的锁。所有wait_zero将返回False。

        :return: (无)
        :rtype: None
        """
        self._op_lock.acquire()
        try:
            self.counter = 0
            self._aborting = True
            self._status_lock.release()
        finally:
            self._op_lock.release()

def generate_hexstring(number: int, lenth: int=8):
    if number < 0:
        raise NotImplementedError()
    return format(number, str(lenth)+"x").replace(" ", "0").upper()