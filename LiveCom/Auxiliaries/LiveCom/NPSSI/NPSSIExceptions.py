from typing import Optional


class NoSuchTerminalError(Exception):
    def __init__(self, terminal_name: str):
        self.terminal_name = terminal_name

    def __str__(self):
        return "No such terminal: " + self.terminal_name


class NoSuchProcessError(Exception):
    def __init__(self, process_id: str):
        self.process_id = process_id

    def __str__(self):
        return "No such process: " +self.process_id


class NotOurBusinessError(Exception):
    def __init__(self, app_name):
        self.app_name = app_name

    def __str__(self):
        return "Attempt to load a app that not for our business: " + self.app_name

