from queue import Queue, Empty
from typing import Callable
from time import sleep
from threading import Lock, Thread
from gc import collect
from LiveCom.Auxiliaries.LiveCom.CommonExcptions import NotAvailableError

class StreamObjectBuffer(object):
    """
    流式对象缓存

    用于需要在间隔特定时间内返回内容缓冲区全部内容的一种缓存对象。
    由于是流式缓存，因此确保了进入顺序与送出顺序的一致性。
    需要使用回调函数来接收返回的内容。
    """
    def __init__(self, callback: Callable, alarm_time: float=0.5, maxsize: int=0,
                 name: str="StreamObjectBuffer"):
        """
        创建流式对象缓存

        :param callback: 回调函数。此函数应当仅接受一个类型为list的参数。
        :param alarm_time: 刷新时间。每隔指定时间，如果缓存中有新内容可用，则返回这些内容。
        :param maxsize: 等待缓冲区最大条目数量。指定为0时表示没有最大值。
        :param name: 本缓存对象的名称。
        """
        self._queue = Queue(maxsize)
        self._alarm_set = False
        self.callback = callback
        self._action_lock = Lock()
        self._alarm_thread = Thread()
        self.name = name
        self.alarm_time = alarm_time
        self._alarm_lock = Lock()

        self._collected = 0
        self._available = True
        pass

    def add_content(self, content) -> None:
        """
        向缓存中加入新内容

        :param content: 要加入的新内容。
        :return: (无)
        :rtype: None
        """
        # serialize requests
        #print("content=" + content +"  lock=" + str(self._action_lock.locked()))
        self._action_lock.acquire()
        if self._available:
            self._queue.put(content)
            if self._alarm_set:
                self._action_lock.release()
                return
            else:
                # create new alarm thread
                #print("Creating new thread.")
                self._alarm_thread = Thread(target=self._timer_thread,
                                            name=self.name+" Alarm Worker")
                #print("Starting new thread.")
                self._alarm_thread.start()
                # lock release is in _alarm_callback.
        else:
            raise NotAvailableError(self)
        pass

    def push_content(self) -> None:
        """
        立即将缓冲区的内容取出。

        取出的内容将会通过回调函数返回。

        :return: (无)
        :rtype: None
        """
        send_list = list()
        while not self._queue.empty():
            try:
                send_list.append(self._queue.get_nowait())
                self._queue.task_done()
            except Empty:
                break

        if not len(send_list) == 0:
            self.callback(send_list)
        self._collected += collect()
        pass

    def _timer_thread(self):
        if not self._alarm_set:
            if not self._alarm_lock.acquire(timeout=1):
                assert "Alarm lock malfunction: locked by someone else."
            self._alarm_set = True
            # remote unlock here.
            self._action_lock.release()
            sleep(self.alarm_time)
            # wait done. callback.
            self._alarm_set = False
            self._alarm_lock.release()
            self.push_content()


        return
        pass

    def wait_timer_task_done(self):
        if self._alarm_set:
            self._alarm_lock.acquire()
            self._alarm_lock.release()
        pass

    def __del__(self):
        self._available = False
        while not self._queue.empty():
            self._queue.join()
        pass



