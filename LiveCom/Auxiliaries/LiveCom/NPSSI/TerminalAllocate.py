from random import randint
from LiveCom.Auxiliaries.LiveCom.MessageTargetUtils import conv_dataset
from LiveCom.Auxiliaries.LiveCom.NPSSI.NPSSIExceptions import NoSuchTerminalError

def generate_hexstring(number: int, lenth: int=8):
    if number < 0:
        raise NotImplementedError()
    return format(number, str(lenth)+"x").replace(" ", "0").upper()

def get_terminal_id(terminal_name: str):
    backend, terminal_count, dummy = conv_dataset(terminal_name)
    if not backend == "NPSSI":
        raise NoSuchTerminalError(terminal_name)

    return int(terminal_count, base=16)
    pass

def allocate_next_terminal(refs: dict):
    for i in range(0, 0xFFFF+1):
        value = "NPSSI:" + generate_hexstring(i, 4)
        if value in refs.keys():
            continue
        else:
            return value
    raise ValueError("No more available terminal to allocate.")
    pass