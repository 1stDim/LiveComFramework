import subprocess
import shlex
from threading import Thread, Lock
import os
import sys
from traceback import print_exc
from typing import Callable, Optional, Iterable, Union

class ConsoleIO(object):
    def __init__(self, cmdline: Union[str, list], stdout_handler, stderr_handler,
                 name: str = "Unnamed Task", start_at_once: bool=False, working_dir=None,
                 redirect_stderr_to_stdout: bool=False, env: Optional[dict]=None, process_stop_handler_info: Optional[dict]=None,
                 encoding: str=None, end_of_line: str=None):
        '''
        创建控制台IO对象

        --------------

        使用 **subprocess** 库创建子进程对象，并绑定相关处理函数。


        **处理函数调用约定**

        用于处理标准输出与标准错误的函数应当只接受1个外部参数，该参数将会是远端发送来的一行输出消息。

        用于处理退出事件的函数可以是任意函数，但填写处理函数信息时应传入一个固定格式的字典(dict)对象：

            |    {
            |        "handler": Callable(),  # 一个可被调用的函数。
            |        "args"： (arg_1, arg_2, arg_3),  # 顺序指定参数列表
            |        "kwargs": {"arg_4": "val_4"|  # 键值对参数列表
            |    }


        在子进程退出之后，这些参数将会传给所指定的函数。

        --------------

        :param cmdline: 要执行的命令行。如果传入的是一个可迭代对象（列表、元组），则作为参数列表使用(需要包括arg0，也就是可执行文件的启动位置信息。)
        :param stdout_handler: 标准输出处理函数。（详见“处理函数调用约定”） 如果该项为空，将会启用调试模式，启动调试用控制台。
        :param stderr_handler: 标准错误处理函数。（详见“处理函数调用约定”） 如果该项为空，将会丢弃标准错误的输出。
        :param name: ConsoleIO 对象的名称。 默认为“Unnamed Task”。
        :param start_at_once: 指定在ConsoleIO类创建时立即启动子进程。
        :param working_dir: 目标进程的起始工作目录。留空为当前目录。
        :param redirect_stderr_to_stdout: 指定是否将标准错误的信息合并进标准输出中。指定为True时，stderr_handler参数将会被忽略。
        :param env: 指定目标进程的环境变量。不指定时将会继承当前进程的环境变量。该指定操作将会是目标进程可用的全部环境变量。
        :param process_stop_handler_info: 指定进程退出事件的处理函数的信息。（详见“处理函数调用约定”）
        :param encoding: 指定字符输入输出的编码字符。如果不指定，在Windows平台将会使用GBK编码，其他平台使用UTF-8编码。
        :param end_of_line: 指定换行符类型。如果不指定，将会根据当前平台的设置进行(Windows为"\\r\\n"，POSIX平台为"\\n")。
        '''

        # make a subprocess object
        self.name = name
        cmdargs = None
        proc_cmdline = ""
        if isinstance(cmdline, str):
            cmdargs = shlex.split(cmdline)
            proc_cmdline = cmdline
        elif isinstance(cmdline, Iterable):
            cmdargs = list(cmdline)
            for arg in cmdargs:
                writer = arg
                if " " in arg:
                    if '"' in arg:
                        writer = writer.replace('"', '\"')
                    writer = '"' + writer + '"'
                proc_cmdline += " " + writer
            proc_cmdline = proc_cmdline[1:-1]
        else:
            raise TypeError('cmdline should be a str or a Iterable.')

        self.cmdargs = cmdargs
        self.working_dir = working_dir
        self._cmdline = proc_cmdline
        self.env = env



        self._subprocess = None
        use_console_display = False
        if stdout_handler is None:
            use_console_display = True
            self.__locale_console_object__ = console_display(self)
            stdout_handler = self.__locale_console_object__.show_stdout
            stderr_handler = self.__locale_console_object__.show_stderr
            redirect_stderr_to_stdout = False
            process_stop_handler_info = {
                "handler": self.__locale_console_object__.exit_handler,
                "args": (),
                "kwargs": {}
            }

        self.redirect_stderr_to_stdout = redirect_stderr_to_stdout
        self._stdout_handler = stdout_handler
        self._stderr_handler = stderr_handler
        self._process_stop_handler_info = process_stop_handler_info
        self.started = False
        self.completed = False

        if encoding is None:
            if sys.platform == 'win32':
                encoding = 'gbk'
            else:
                encoding = 'utf-8'


        self.encoding = encoding

        if end_of_line is None:
            end_of_line = os.linesep

        self.end_of_line = end_of_line
        self.returncode = None

        # import _io
        # self._fd_stdin = _io.BufferedWriter()
        # self._fd_stdout = _io.BufferedReader()
        # self._fd_stderr = _io.BufferedReader()
        # self._subprocess = subprocess.Popen()

        self._thread_stdout_worker = Thread(target=self._process_stdout_worker, daemon=True,
                                            name="ConsoleIO " + name + " stdout worker")

        if not redirect_stderr_to_stdout:
            self._thread_stderr_worker = Thread(target=self._process_stderr_worker, daemon=True,
                                                name="ConsoleIO " + name + " stderr worker")

        self._output_locker = Lock()

        if start_at_once:
            self.start()

        if use_console_display:
            self.__locale_console_object__.start_with_stdin_sender(not start_at_once)

    def __repr__(self):
        base = "<ConsoleIO '" + self.name + "' cmdline='" + self._cmdline + "' "
        if self.started:
            if not self.completed:
                base += "started "
            else:
                base += "finished "
        else:
            base += "obj_created "

        base += "at " + str(hex(id(self))) + ">"
        return base  # I'm not a plane.

    def start(self):
        if self.redirect_stderr_to_stdout:
            err_settings = subprocess.STDOUT
        else:
            if self._stderr_handler is None:
                err_settings = subprocess.DEVNULL
            else:
                err_settings = subprocess.PIPE

        self._subprocess = subprocess.Popen(self.cmdargs,
                                            stdout=subprocess.PIPE,
                                            stderr=err_settings,
                                            stdin=subprocess.PIPE,
                                            cwd=self.working_dir,
                                            bufsize=1,
                                            universal_newlines=False,
                                            env=self.env)
        self.started = True

        # start worker threads.
        if self.redirect_stderr_to_stdout:
            self._thread_stdout_worker.start()
        else:
            self._thread_stdout_worker.start()
            self._thread_stderr_worker.start()
        pass

    def terminate(self):
        if self.started:
            self._subprocess.terminate()
        else:
            raise NotStartedException(self)
        pass

    def kill(self):
        if self.started:
            self._subprocess.kill()
        else:
            raise NotStartedException(self)
        pass

    def _text_preproc(self, string: str):
        string.replace("\r\n", "\n")
        string.replace("\r", "\n")
        string.replace("\n", self.end_of_line)
        #  string.replace("\n", os.linesep)
        return string

    def writeline(self, string: str):
        if self.started:
            # universally remove linesep.
            if not self.completed:
                #print(str(self._text_preproc(string).rstrip("\n")+ "\n").__repr__())
                self._subprocess.stdin.write((self._text_preproc(string).rstrip(self.end_of_line)
                 + self.end_of_line).encode(self.encoding))
                self._subprocess.stdin.flush()
            # we ensure there will be a EOL.
        else:
            raise NotStartedException(self)

    def write(self, string: str):
        if self.started:
            if not self.completed:
                self._subprocess.stdin.write(self._text_preproc(string).encode(self.encoding))
                self._subprocess.stdin.flush()
        else:
            raise NotStartedException(self)

    def is_running(self):
        return self.started and not self.completed

    def __cleanup_killall__(self):
        if not self.started:
            pass
        else:
            self._subprocess.kill()

    def _process_stdout_worker(self):
        # always need.
        if self.started:
            while self._subprocess.poll() is None:
                try:
                    data = self._subprocess.stdout.readline().decode(self.encoding)
                except UnicodeDecodeError:
                    print_exc()
                if len(str(data)) == 0:
                     continue
                # get lock first
                #if not self.completed:
                self._output_locker.acquire()
                # do average things
                self._stdout_handler(str(data).rstrip(self.end_of_line))
                # done, release lock
                self._output_locker.release()
            # task finished
            self.completed = True
            self.returncode = self._subprocess.returncode
            pass
            # call exit handler
            if self._process_stop_handler_info is None:
                # do nothing.
                return
            else:
                handler = self._process_stop_handler_info["handler"]
                args = self._process_stop_handler_info["args"]
                kwargs = self._process_stop_handler_info["kwargs"]
                handler(*args, **kwargs)
        else:
            raise NotStartedException(self)
        pass

    def _process_stderr_worker(self):
        # do not start me if there is no need.
        if self.redirect_stderr_to_stdout:
            print("Debug: there should not use _process_stderr_worker here.")
            return
        if self.started:
            while self._subprocess.poll() is None:
                try:
                    data = self._subprocess.stderr.readline().decode(self.encoding)
                except UnicodeDecodeError:
                    print_exc()
                if len(str(data)) == 0:
                    continue
                # get lock first
                #if not self.completed:
                self._output_locker.acquire()
                # do average things
                self._stderr_handler(str(data).rstrip(self.end_of_line))
                # done, release lock
                self._output_locker.release()

            # task finished
            self.completed = True
            pass
        else:
            raise NotStartedException(self)
        pass
    def _dummy(self, context: str):
        pass

class NotStartedException(Exception):
    def __init__(self, console_io: ConsoleIO):
        self.target_console_io = console_io

    def __str__(self):
        return "This ConsoleIO object has not been started yet: " + \
            self.target_console_io.__repr__()


class console_display(object):
    def __init__(self, console_io: ConsoleIO, console_encoding:str = None):
        if console_encoding is None:
            if sys.platform == 'win32':
                console_encoding = "gbk"
            else:
                console_encoding = "utf-8"
        self.encoding = console_encoding
        self.console_io = console_io

    def show_stdout(self, context: str):
        print("OUT|" + context)  # , end="")
        pass

    def show_stderr(self, context: str):
        print("ERR|" + context)  # , end="")
        pass

    def exit_handler(self):
        print("Process exited with code: " + str(self.console_io.returncode))
        pass

    def start_with_stdin_sender(self, do_start=True):
        if do_start:
            self.console_io.start()
        print("starting console")
        while not self.console_io.completed:
            var = sys.stdin.readline()
            self.console_io.writeline(var)
        print("stopping console")
