from LiveCom.Auxiliaries.LiveCom.SystemInfo import getSystemVersion, getLastSupportedVersion

def ModuleInfo():
    return {
        "name": "LiveComFW",
        "description": "LiveCom 框架核心",
        "version": getSystemVersion(),
        "deprecated": getLastSupportedVersion(),
        "require": [
            # nothing required.
        ]
    }

def Setup(sysbus):
    # simply do nothing.
    pass

def Takedown(sysbus):
    # do nothing at all
    # if take me down, that means system shutdown
    print("LiveCom: 反初始化: 正在退出")
    import sys
    sys.exit(0)
    pass