import sys
def showLogo():
    print(
        '''
        01001100 01101001 01110110 01100101 01000011 01101111 01101101

         d8        oo                    a88888b.
         88                             d8'   `88
         88        dP dP   .dP .d8888b. 88        .d8888b. 8.d8b.d8b.
         88        88 88   d8' 88ooood8 88        88'  `88 88'`88'`88
         88        88 88 .88'  88.  ... Y8.   .88 88.  .88 88  88  88
         88888888P dP 8888P'   `88888P'  Y88888P' `88888P' dP  dP  dP

        01001100 01101001 01110110 01100101 01000011 01101111 01101101


    LiveCom IM Service Platform     (C) 2017-2018 1stDim SocEngine Project
        ''')

def getSystemVersion():
    return "0.1"

def getLastSupportedVersion():
    return "0.1"

def getPythonVersionInfo():
    a = sys.version.split(" ", maxsplit=1)
    b = a[1].split(")", maxsplit=1)
    c = b[1].split("[")
    verstr = a[0]
    detailver = b[0].lstrip("(")
    compiler = c[1].rstrip("]")
    return verstr, detailver, compiler

def getCopyrightInfo():
    verstr, detailver, compiler = getPythonVersionInfo()
    return """\
LiveCom IM Service Platform
Version """ + getSystemVersion() + """
(C) 2017-2018  1stDim SocEngine Project

WARNING: This is a Beta stage software.
         Please use with caution.

# Python Environment\n""" + \
"Python Version: " + verstr + "\n   " + \
           detailver + "\n   " + \
           compiler + "\n" + \
"Platform: " + sys.platform + """

# Staff
Lead Programmer  \t: SuperMarioSF

# Special Thanks
CQHTTP Python SDK\t: richardchien"""


def _LetsRememberThatOldDays():
    return """\
[LiveCom | We are Busters!]
We are all friends, from the begining.
\"We are Busters!\" that is what we said.
Though nothing could stay forever, we can still remember that old days.

From the origin of LiveOS of the Little Busters! Live Project
LiveCom: the rebirth of the LiveOS x86 Plan

Thanks for everyone who joined into the LBLive.
You all are the best friends, thank you."""