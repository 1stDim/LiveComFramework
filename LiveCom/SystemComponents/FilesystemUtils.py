import os


def conv_path_to_list(path: str) -> list:
    uplvl, curlvl = os.path.split(path)
    if uplvl == "":
        if curlvl == "":
            return []
        return [curlvl]
    if curlvl == "":
        return [uplvl]
    rlist = conv_path_to_list(uplvl)
    rlist.append(curlvl)
    return rlist


def conv_list_to_path(pathlist: list) -> str:
    basename = ""
    for name in pathlist:
        if basename == "":
            basename += name
        else:
            basename += os.path.sep + name
    return basename


def mkdir_recursive(path):
    basename = ""
    for name in conv_path_to_list(path):
        if basename == "":
            basename += name
        else:
            basename += os.path.sep + name
        try:
            os.mkdir(basename)
        except FileExistsError:
            # expected.
            pass
        except FileNotFoundError as e:
            # impossible path?!
            t = list(e.args)
            t.append(path)
            r = tuple(t)
            raise FileNotFoundError(*r) from e
