import queue as queues
from threading import Thread, ThreadError
from time import sleep, mktime, time
from typing import Callable, Optional, List, Tuple, Iterable
import gc
import random
from LiveCom.SystemComponents.API import APIManager

class EventBus(object):
    """
    LiveCom Generic Event Bus

    """
    from LiveCom.Auxiliaries.LiveCom.EventBus.EventBusToolkits import EType

    def __init__(self, name: str="DefaultEventBus", incoming_queue_size=0, outgoing_queue_size=0,
                 *, system_bus: bool=False):
        """Initial setup for a EventBus.

        :param str name: EventBus name (Optional non-blank string)
        :param int incoming_queue_size: max incoming queue size, default is unlimited.
        :param int outgoing_queue_size: max outgoing queue size, default is unlimited.
        :param bool system_bus: Indicate the system bus. Only system bus have API Manager.
                                Also, there is only one system bus in whole framework.
        :rtype: EventBus
        """
        # performance info collector
        self._gc_collected_refs = 0

        # do a collect now.
        self._gc_collected_refs += gc.collect()

        # bus tree setup
        self._tree = {
            'lock': {                      # delete-operation lock flag (WIP)
                'incoming': 0,
                'outgoing': 0
            },
            'incoming': [],
            'outgoing': []
        }

        self._token = random.randint(0, 0xFFFFFFFF)

        # name setup
        if not (isinstance(name, str) and len(name) > 0):
            raise TypeError("EventBus name must be a non-blank string.")
        self.name = name

        # queue setup
        self._incoming_queue = queues.Queue(incoming_queue_size)
        self._outgoing_queue = queues.Queue(outgoing_queue_size)

        # thread worker initalize
        self._thread_worker_init()

        # thread runtime mark setup
        self._incoming_thread_run_mark = False
        self._outgoing_thread_run_mark = False
        self._first_started = False
        self._thread_worker_running = False

        if system_bus:
            self.APIManager = APIManager(sysbus=self)

        self.is_sysbus = system_bus

        # TODO: Thread worker operation lock need to be implemented.

    def is_running(self):
        return self._thread_worker_running

    def _thread_worker_init(self):
        # thread setup
        self._incoming_thread = Thread(target=self._incoming_worker, daemon=True,
                                       name=self.name+".incoming_worker")
        self._outgoing_thread = Thread(target=self._outgoing_worker, daemon=True,
                                       name=self.name+".outgoing_worker")

    def __repr__(self):
        return "<EventBus object name='" + str(self.name) + "' at " + str(hex(id(self))) + ">"

    @staticmethod
    def _check_etype(etype: str) -> str:
        """
        Check etype and return unified name.

        :param str etype: EventType
        :return: Unified etype name ('incoming' or 'outgoing')
        :rtype: str
        """
        if not (etype.lower() == "incoming" or
                etype.lower() == "i" or
                etype.lower() == "outgoing" or
                etype.lower() == "o"):
            raise ValueError("Illegal etype: Need to be one of these eType: 'incoming', 'i', 'outgoing', 'o'.")
        else:
            if etype.lower() == "incoming" or etype.lower() == "i":
                return "incoming"
            elif etype.lower() == "outgoing" or etype.lower() == "o":
                return "outgoing"
            else:
                raise ValueError("Something went wrong in EventBus._check_etype: etype = " + str(etype))
        pass

    def _get_group(self, etype: str, group: str) -> Optional[dict]:
        """
        Get a group data reference structure by specified data

        :param etype: EventType
        :param group: Group name
        :return: partial of main _tree object reference for editing. Return None for failed to get group.
        :rtype: dict|None
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified etype
        uetype = self._check_etype(etype)

        # iter all target object to find target
        for i in self._get_group_list(etype=uetype):
            if i['name'] == group:
                return i

        return None
        pass

    def _get_listener(self, etype: str, group: str, name: str) -> Optional[dict]:
        """
        Get a listener data reference structure by specified data

        :param etype: EventType
        :param group: Group name
        :param name: Listener name
        :return: partial of main _tree object reference for editing. Return None for failed to get listener.
        :rtype: dict|None
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified etype
        uetype = self._check_etype(etype)

        # check group is existed
        if not self._check_group_existed(uetype, group):
            return None

        # search listener, locate target group
        for i in self._get_group_list(etype=uetype):
            if i['name'] == group:
                for j in i['listeners']:    # find listener name
                    if j['name'] == name:
                        return j

        return None

    def _get_group_list(self, etype: str) -> Optional[list]:
        """
        Get all groups in list in specified EventType.

        :param etype: EventType
        :return: partial of main _tree object reference for editing. Return None for failed to get etype.
        :rtype: list|None
        """
        # TODO: Query-Delete lock to be implemented.
        try:
            # get unified etype name
            uetype = self._check_etype(etype=etype)
        except ValueError:
            return None

        return self._tree[uetype]
        pass

    def _get_listener_list(self, etype: str, group: str) -> Optional[list]:
        """
        Get all listener in list in specified group.

        :param etype: EventType
        :param group: Group name
        :return: partial of main _tree object reference for editing. Return None for failed to get requested group.
        :rtype: list|None
        """
        # TODO: Query-Delete lock to be implemented.
        try:
            # get unified etype name
            uetype = self._check_etype(etype=etype)
        except ValueError:
            return None

        # check if group existed
        if not self._check_group_existed(etype=uetype, group=group):
            return None

        # return list
        return self._get_group(etype=uetype, group=group)['listeners']
        pass

    def _check_group_existed(self, etype: str, group: str) -> bool:
        """
        Check if group existed in specified etype.

        :return: if is existed in target etype.
        :rtype: bool
        :param str etype: EventType
        :param str group: Group name
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified name
        uetype = self._check_etype(etype)

        return bool(self._get_group(etype=uetype, group=group))
        pass

    def _check_listener_existed(self, etype: str, group: str, name: str) -> bool:
        """
        Check a listener inside a target group.

        **Note:** Please check group existence before use this function.
            or you cannot confirm this 'not found' is caused by a non-existed group.

        :param str etype: EventType
        :param str group: Group name
        :param str name: Listener name
        :return: If this listener or specified group is existed.
        :rtype: bool
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified name
        uetype = self._check_etype(etype)

        return bool(self._get_listener(etype=uetype, group=group, name=name))
        pass

    def create_group(self, etype: str, group: str, *, ignore_existed: bool=False) -> bool:
        """
        Create notification group by given name

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param bool ignore_existed: Default **False**.
                Will throw **GroupAlreadyExistedError** when specified group is existed.
                When set to **True** and specified group is already existed, return **False**.
        :return: If create group operation success.
        :rtype: bool
        """
        # check etype
        uetype = self._check_etype(etype)

        # check if already existed
        if not self._check_group_existed(uetype, group):
            self._get_group_list(uetype).append(
                {
                    "type": "ListenerGroupObject",  # object type
                    "name": group,                   # object name
                    "lock": 0,                  # delete-operation lock flag (WIP)
                    "listeners": []                 # blank listener container
                }
            )
            return True
        else:
            if not ignore_existed:
                raise GroupAlreadyExistedError(etype=etype, grpname=group)
        pass

    def remove_group(self, etype: str, group: str, *, ignore_not_existed: bool=False) -> bool:
        """
        Remove notification group by given name

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param bool ignore_not_existed: Default **False**.
                Will throw **NoSuchGroupError** when specified group is not existed.
                When set to **True** and specified group is not existed, return **False**.
        :return: If remove group operation success.
        :rtype: bool
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified etype name
        uetype = self._check_etype(etype)

        # check group is existed
        if self._check_group_existed(etype=uetype, group=group):
            self._get_group_list(uetype).remove(
                self._get_group(etype=uetype, group=group))  # delete
        else:
            # seems nothing was found.
            if ignore_not_existed:
                return False
            else:
                raise NoSuchGroupError(etype=etype, grpname=group)
            pass

    def add_listener(self, etype: str, group: str, name: str, handler: Callable, *,
                     args: tuple=None, kwargs: dict=None, append_name: str="context",
                     metadata_name: Optional[str]=None,
                     ignore_existed=False, ignore_group_not_existed=False) -> (bool, str):
        """
        Add a new listener with handler option into specified group.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param str name: Listener name
        :param handler: Listener handle for event
        :param tuple args: Additional args for handler function
        :param dict kwargs: Additional kwargs for handler function
        :param str append_name: Arg name for context passing in. (append in kwargs)
        :param str|None metadata_name: Arg name for event metadata passing in. (append in kwargs)
                        Default is None, indicate the listener does not care about metadata.
        :param bool ignore_existed: Default **False**.
                        Will throw **ListenerAlreadyExisted** when specified listener is existed.
                        When set to **True** and specified listener is already existed, return **False**.
        :param ignore_group_not_existed: Default **False**.
                        Will throw **NoSuchGroupError** when specified group is not existed.
                        When set to **True** and specified group is not existed, return **False**.
        :return: if add listener operation success, and if failed, the reason.
                        In tuple '(status: bool, reason: str)'.
                        Reason 'group' indicate the group is not existed;
                        Reason 'listener' indicate the listener is already existed.
        :rtype: (bool, str)
        """
        # get unified etype
        uetype = self._check_etype(etype)

        # check group is existed
        if not self._check_group_existed(etype=uetype, group=group):
            if ignore_group_not_existed:
                return False, 'group'
            else:
                raise NoSuchGroupError(etype=etype, grpname=group)

        # seems group is existed. so check listener is existed.
        if self._check_listener_existed(etype=uetype, group=group, name=name):
            if ignore_existed:
                return False, 'listener'
            else:
                raise ListenerAlreadyExistedError(etype=etype, grpname=group, listenername=name)

        # seems OK for add a listener.

        # check data types
        if args is None:
            args = tuple()
        if kwargs is None:
            kwargs = dict()

        if not isinstance(args, tuple):
            raise TypeError("args must be a tuple or None.")

        if not isinstance(kwargs, dict):
            raise TypeError("kwargs must be a dict or None.")

        if not (isinstance(append_name, str) and len(append_name) > 0):
            raise TypeError("append_name must be a non-blank string.")

        if metadata_name is not None:
            if not (isinstance(metadata_name, str) and len(metadata_name) > 0):
                raise TypeError("metadata_name must be a non-blank string, or be None.")

        if not isinstance(handler, Callable):
            raise TypeError("handler must be a callable (function or class method).")

        # check OK

        self._get_listener_list(etype=uetype, group=group).append(
            {
                "type": "ListenerObject",
                "name": name,
                "handler": handler,
                "args": args,
                "kwargs": kwargs,
                "append_name": append_name,
                "metadata_name": metadata_name
            }
        )

        return True, ''
        pass

    def del_listener(self, etype: str, group: str, name: str,
                     ignore_not_existed=False, ignore_group_not_existed=False) -> (bool, str):
        """
        Delete a listener from a given group.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param str name: Listener name
        :param bool ignore_not_existed: Default **False**.
                        Will throw **NoSuchListenerError** when specified listener is not existed.
                        When set to **True** and specified listener is not existed, return **False**.
        :param ignore_group_not_existed: Default **False**.
                        Will throw **NoSuchGroupError** when specified group is not existed.
                        When set to **True** and specified group is not existed, return **False**.
        :return: if delete listener operation success, and if failed, the reason.
                        In tuple '(status: bool, reason: str)'.
                        Reason 'group' indicate the group is not existed;
                        Reason 'listener' indicate the listener is not existed.
        :rtype: (bool, str)
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified etype name
        uetype = self._check_etype(etype)

        # check group is existed
        if not self._check_group_existed(etype=uetype, group=group):
            if ignore_group_not_existed:
                return False, 'group'
            else:
                raise NoSuchGroupError(etype=etype, grpname=group)
            pass

        # check listener is existed
        if not self._check_listener_existed(etype=uetype, group=group, name=name):
            if ignore_not_existed:
                return False, "listener"
            else:
                raise NoSuchListenerError(etype=etype, grpname=group, listenername=name)

        # do delete operation
        self._get_listener_list(etype=uetype, group=group).remove(
            self._get_listener(etype=uetype, group=group, name=name))  # delete

        return True, ''
        pass

    def get_listeners_from_group(self, etype: str, group: str) -> list:
        """
        List all listener names from given group name.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :return: List of listener names.
        :rtype: list
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified etype name
        uetype = self._check_etype(etype)

        # check group existance
        if not self._check_group_existed(etype=etype, group=group):
            raise NoSuchGroupError(etype=etype, grpname=group)

        # initialize base list
        lst = list()

        # construct list
        for i in self._get_listener_list(etype=uetype, group=group):
            lst.append(i['name'])

        # task finish.
        return lst
        pass

    def get_groups_by_etype(self, etype: str) -> list:
        """
        List all group names from given etype name.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :return: List of group names.
        :rtype: list
        """
        # TODO: Query-Delete lock to be implemented.
        # get unified etype name
        uetype = self._check_etype(etype)

        # initialize base list
        lst = list()

        # construct list
        for i in self._get_group_list(etype=etype):
            lst.append(i['name'])

        # task finish.
        return lst
        pass

    def get_all_groups(self) -> List[Tuple[str, str]]:
        """
        List all group names in message bus.

        :return: List of group names.
                    In tuple: '(name, etype)'.
                    etype can be either "incoming" or "outgoing", indicating event group type.
        :rtype: list((str,str), ...)
        """
        # TODO: Query-Delete lock to be implemented.
        lst = list()

        for i in self.get_groups_by_etype(etype="incoming"):
            lst.append((i, "incoming"))

        for o in self.get_groups_by_etype(etype="outgoing"):
            lst.append((o, "outgoing"))

        return lst
        pass

    # from now on, methods below is for scheudler jobs.

    def start_loop(self):
        # TODO: Thread worker operation lock need to be implemented.
        # check if first start: re-initialize thread when first started
        if self._first_started:
            self._thread_worker_init()

        # mark first start
        self._first_started = True

        # set mark to start
        self._incoming_thread_run_mark = True
        self._outgoing_thread_run_mark = True

        # do a cleanup first
        self._gc_collected_refs += gc.collect()

        # start worker
        self._incoming_thread.start()
        self._outgoing_thread.start()

        # mark startup status
        self._thread_worker_running = True
        pass

    def stop_loop(self, *, wait_timeout=None):
        # TODO: Thread worker operation lock need to be implemented.
        # no operation if thread not even started
        if not self._first_started:
            raise EventBusError("Worker not even started for first time!")

        # set mark to start
        self._incoming_thread_run_mark = False
        self._outgoing_thread_run_mark = False

        # ask for stop (by sending stop signal)
        self._create_stop_signal()

        # join to thread
        self._incoming_thread.join(timeout=wait_timeout)
        self._outgoing_thread.join(timeout=wait_timeout)

        # mark stop status
        self._thread_worker_running = False

        # finalize
        self._token = random.randint(0, 0xFFFFFFFF)

    def create_event(self, etype: str, source: str,
                     target_groups: Iterable = None, context: dict = None, *,
                     typename: str = "BasicEvent",
                     ignore_not_existed_groups: bool = False) -> None:
        """

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str source: Indicate where does this event come from. Provided for listeners.
        :param Iterable target_groups: A list of listener group names.
        :param dict context: Context for listeners to receive.
        :param str typename: Custom event type name. Provided for listeners
        :param bool ignore_not_existed_groups: If set to True, will ignore non-existed groups in **target_groups**.
                Default False, will throw **NoSochGroupError** if one of **target_group** is not existed.
        """
        self._create_new_event(etype=etype, source=source, context=context,
                               target_groups=target_groups, typename=typename,
                               ignore_not_existed_groups=ignore_not_existed_groups)
        pass


    def _create_new_event(self, *, etype: str, source: str,
                          target_groups: list=None, context: dict=None,
                          typename: str="BasicEvent", token: int=None,
                          ignore_not_existed_groups=False):
        # get unified etype name
        uetype = self._check_etype(etype=etype)

        # unified input from None
        if target_groups is None: target_groups = []
        if context is None: context = {}

        # check input
        if not isinstance(source, str): raise TypeError("source must be a str.")
        if len(source) == 0: raise ValueError("source should not be blank.")
        if not isinstance(target_groups, Iterable): raise TypeError("target must be a Iterable (such as list or tuple).")
        if not isinstance(context, dict): raise TypeError("context must be a dict.")
        if not isinstance(typename, str): raise TypeError("typename must be a str.")
        if len(typename) == 0: raise ValueError("typename should not be blank.")
        if token is not None:
            if not isinstance(token, int): raise TypeError("token must be a int.")

        # check group existance
        unified_target_groups = []
        for grpname in target_groups:
            if self._check_group_existed(uetype, grpname):
                unified_target_groups.append(grpname)
            else:
                if not ignore_not_existed_groups:
                    raise NoSuchGroupError(etype=etype, grpname=grpname)

        event_struct = {
            "type": typename,
            "timestamp": time(),
            "source": source,
            "target_groups": unified_target_groups,
            "context": context
        }

        if token is not None:
            event_struct["token"] = token

        if uetype == "incoming":
            self._incoming_queue.put(event_struct)
        elif uetype == "outgoing":
            self._outgoing_queue.put(event_struct)
        else:
            assert "CreateEvent failed due to unknown uetype."

        pass

    def _create_stop_signal(self):
        # send stop signal to both queue
        self._create_new_event(etype="incoming", source=self._get_eventbus_local_source_name(),
                               token=self._token, typename="EventBusStopSignal")
        self._create_new_event(etype="outgoing", source=self._get_eventbus_local_source_name(),
                               token=self._token, typename="EventBusStopSignal")
        pass

    def _get_eventbus_local_source_name(self):
        return "EventBus:" + self.name

    def _incoming_worker(self):
        # this method is designed with Event_Structure.py
        while True:
            event = self._incoming_queue.get()
            # check if the exit flag correct
            if event["type"] == "EventBusStopSignal" and \
                    event["source"] == self._get_eventbus_local_source_name() and \
                    event["token"] == self._token:
                self._gc_collected_refs += gc.collect()  # do some cleanup.
                self._incoming_queue.task_done()
                break  # exit from loop

            # worker begin
            self._event_processor(etype="incoming", event=event)
            self._gc_collected_refs += gc.collect()  # do some cleanup.
            self._incoming_queue.task_done()

        pass

    def _outgoing_worker(self):
        # this method is designed with Event_Structure.py
        while True:
            event = self._outgoing_queue.get()
            # check if the exit flag correct
            if event["type"] == "EventBusStopSignal" and \
                    event["source"] == self._get_eventbus_local_source_name() and \
                    event["token"] == self._token:
                self._gc_collected_refs += gc.collect()  # do some cleanup.
                self._outgoing_queue.task_done()
                break  # exit from loop

            # worker begin
            self._event_processor(etype="outgoing", event=event)
            self._gc_collected_refs += gc.collect()  # do some cleanup.
            self._outgoing_queue.task_done()

        pass

    def _event_processor(self, etype: str, event: dict):
        # get unified etype name
        uetype = self._check_etype(etype=etype)
        # perform event broadcast
        # step 1: match groups
        egrouplist = []
        try:
            for grpname in event["target_groups"]:
                egrouplist.append(self._get_group(etype=uetype, group=grpname))
        except KeyError as e:
            raise NoSuchGroupError(etype=etype, grpname=str(e))

        # do broadcast
        # TODO: multi-thread is required here. (performance boost)
        thread_list = []
        for grp in egrouplist:
            for listener_structs in grp["listeners"]:
                # one-step start?
                kwargs_push = listener_structs["kwargs"]
                kwargs_push[listener_structs["append_name"]] = event["context"]

                if listener_structs["metadata_name"] is not None:
                    event_copy=dict(event)
                    event_copy.pop("context")
                    event_copy["etype"] = uetype
                    kwargs_push[listener_structs["metadata_name"]] = event_copy

                # rock 'in roll!
                thread_list.append(Thread(target=listener_structs["handler"],
                                          args=listener_structs["args"],
                                          kwargs=kwargs_push))

        for run in thread_list:
            run.start()  # start everything

        # I hate this join(), you know.
        # I spend almost 5 hours on this.
        # This join() take me a deadlock.
        # F************K!! (sorry for bad words)

        # for join in thread_list:
        #     join.join()  # wait for everything done. <- Lock everything up

        pass


class EventBusError(Exception):
    pass


class NoSuchGroupError(Exception):
    def __init__(self, etype, grpname):
        self.etype=etype
        self.group_name=grpname

    def __str__(self):
        return "Group not found: [" + str(self.etype).upper() + "] " + str(self.group_name)


class NoSuchListenerError(Exception):
    def __init__(self, etype, grpname, listenername):
        self.etype=etype
        self.group_name=grpname
        self.listener_name=listenername

    def __str__(self):
        return "Listener not found: [" + str(self.etype).upper() + "] " + \
               str(self.listener_name) + "@" + str(self.group_name)


class GroupAlreadyExistedError(Exception):
    def __init__(self, etype, grpname):
        self.etype=etype
        self.group_name=grpname

    def __str__(self):
        return "Group already existed: [" + str(self.etype).upper() + "] " + str(self.group_name)


class ListenerAlreadyExistedError(Exception):
    def __init__(self, etype, grpname, listenername):
        self.etype=etype
        self.group_name=grpname
        self.listener_name=listenername

    def __str__(self):
        return "Listener already existed: [" + str(self.etype).upper() + "] " + \
               str(self.listener_name) + "@" + str(self.group_name)
