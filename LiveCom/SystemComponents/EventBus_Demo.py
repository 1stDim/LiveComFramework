from typing import Callable, Optional, List, Tuple, Iterable

class EventBus(object):
    """
    LiveCom Generic Event Bus

    """

    from LiveCom.Auxiliaries.LiveCom.EventBus.EventBusToolkits import EType

    def __init__(self, name: str="DemoBus", incoming_queue_size=0, outgoing_queue_size=0,
                 *, system_bus: bool=False):
        """Initial setup for a EventBus.

        :param str name: EventBus name (Optional non-blank string)
        :param int incoming_queue_size: max incoming queue size, default is unlimited.
        :param int outgoing_queue_size: max outgoing queue size, default is unlimited.
        :param bool system_bus: Indicate the system bus. Only system bus have API Manager.
                                Also, there is only one system bus in whole framework.
        :rtype: EventBus
        """
        # performance info collector
        self._gc_collected_refs = 0
        # bus tree setup
        self._tree = {
            'lock': {                      # delete-operation lock flag (WIP)
                'incoming': 0,
                'outgoing': 0
            },
            'incoming': [],
            'outgoing': []
        }

        self._token = 0xFFFFFFFF
        self.name = name

        # queue setup
        self._incoming_queue = None
        self._outgoing_queue = None

        # thread worker initalize
        self._thread_worker_init()

        # thread runtime mark setup
        self._incoming_thread_run_mark = False
        self._outgoing_thread_run_mark = False
        self._first_started = False
        self._thread_worker_running = False

        if system_bus:
            self.APIManager = None

        self.is_sysbus = system_bus
        # TODO: Thread worker operation lock need to be implemented.

    def is_running(self):
        return self._thread_worker_running

    def _thread_worker_init(self):
        pass

    def __repr__(self):
        return "<EventBus Demo object name='" + str(self.name) + "' at " + str(hex(id(self))) + ">"

    @staticmethod
    def _check_etype(etype: str) -> str:
        """
        Check etype and return unified name.

        :param str etype: EventType
        :return: Unified etype name ('incoming' or 'outgoing')
        :rtype: str
        """
        if not (etype.lower() == "incoming" or
                etype.lower() == "i" or
                etype.lower() == "outgoing" or
                etype.lower() == "o"):
            raise ValueError("Illegal etype: Need to be one of these eType: 'incoming', 'i', 'outgoing', 'o'.")
        else:
            if etype.lower() == "incoming" or etype.lower() == "i":
                return "incoming"
            elif etype.lower() == "outgoing" or etype.lower() == "o":
                return "outgoing"
            else:
                raise ValueError("Something went wrong in EventBus._check_etype: etype = " + str(etype))
        pass

    def _get_group(self, etype: str, group: str) -> Optional[dict]:
        """
        Get a group data reference structure by specified data

        :param etype: EventType
        :param group: Group name
        :return: partial of main _tree object reference for editing. Return None for failed to get group.
        :rtype: dict|None
        """
        pass

    def _get_listener(self, etype: str, group: str, name: str) -> Optional[dict]:
        """
        Get a listener data reference structure by specified data

        :param etype: EventType
        :param group: Group name
        :param name: Listener name
        :return: partial of main _tree object reference for editing. Return None for failed to get listener.
        :rtype: dict|None
        """
        pass

    def _get_group_list(self, etype: str) -> Optional[list]:
        """
        Get all groups in list in specified EventType.

        :param etype: EventType
        :return: partial of main _tree object reference for editing. Return None for failed to get etype.
        :rtype: list|None
        """
        pass

    def _get_listener_list(self, etype: str, group: str) -> Optional[list]:
        """
        Get all listener in list in specified group.

        :param etype: EventType
        :param group: Group name
        :return: partial of main _tree object reference for editing. Return None for failed to get requested group.
        :rtype: list|None
        """
        pass

    def _check_group_existed(self, etype: str, group: str) -> bool:
        """
        Check if group existed in specified etype.

        :return: if is existed in target etype.
        :rtype: bool
        :param str etype: EventType
        :param str group: Group name
        """
        pass

    def _check_listener_existed(self, etype: str, group: str, name: str) -> bool:
        """
        Check a listener inside a target group.

        **Note:** Please check group existence before use this function.
            or you cannot confirm this 'not found' is caused by a non-existed group.

        :param str etype: EventType
        :param str group: Group name
        :param str name: Listener name
        :return: If this listener or specified group is existed.
        :rtype: bool
        """
        pass

    def create_group(self, etype: str, group: str, *, ignore_existed: bool=False) -> bool:
        """
        Create notification group by given name

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param bool ignore_existed: Default **False**.
                Will throw **GroupAlreadyExistedError** when specified group is existed.
                When set to **True** and specified group is already existed, return **False**.
        :return: If create group operation success.
        :rtype: bool
        """
        pass

    def remove_group(self, etype: str, group: str, *, ignore_not_existed: bool=False) -> bool:
        """
        Remove notification group by given name

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param bool ignore_not_existed: Default **False**.
                Will throw **NoSuchGroupError** when specified group is not existed.
                When set to **True** and specified group is not existed, return **False**.
        :return: If remove group operation success.
        :rtype: bool
        """
        pass

    def add_listener(self, etype: str, group: str, name: str, handler: Callable, *,
                     args: tuple=None, kwargs: dict=None, append_name: str="context",
                     metadata_name: Optional[str]=None,
                     ignore_existed=False, ignore_group_not_existed=False) -> (bool, str):
        """
        Add a new listener with handler option into specified group.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param str name: Listener name
        :param handler: Listener handle for event
        :param tuple args: Additional args for handler function
        :param dict kwargs: Additional kwargs for handler function
        :param str append_name: Arg name for context passing in. (append in kwargs)
        :param str|None metadata_name: Arg name for event metadata passing in. (append in kwargs)
                        Default is None, indicate the listener does not care about metadata.
        :param bool ignore_existed: Default **False**.
                        Will throw **ListenerAlreadyExisted** when specified listener is existed.
                        When set to **True** and specified listener is already existed, return **False**.
        :param ignore_group_not_existed: Default **False**.
                        Will throw **NoSuchGroupError** when specified group is not existed.
                        When set to **True** and specified group is not existed, return **False**.
        :return: if add listener operation success, and if failed, the reason.
                        In tuple '(status: bool, reason: str)'.
                        Reason 'group' indicate the group is not existed;
                        Reason 'listener' indicate the listener is already existed.
        :rtype: (bool, str)
        """
        pass

    def del_listener(self, etype: str, group: str, name: str,
                     ignore_not_existed=False, ignore_group_not_existed=False) -> (bool, str):
        """
        Delete a listener from a given group.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :param str name: Listener name
        :param bool ignore_not_existed: Default **False**.
                        Will throw **NoSuchListenerError** when specified listener is not existed.
                        When set to **True** and specified listener is not existed, return **False**.
        :param ignore_group_not_existed: Default **False**.
                        Will throw **NoSuchGroupError** when specified group is not existed.
                        When set to **True** and specified group is not existed, return **False**.
        :return: if delete listener operation success, and if failed, the reason.
                        In tuple '(status: bool, reason: str)'.
                        Reason 'group' indicate the group is not existed;
                        Reason 'listener' indicate the listener is not existed.
        :rtype: (bool, str)
        """
        pass

    def get_listeners_from_group(self, etype: str, group: str) -> list:
        """
        List all listener names from given group name.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str group: Group name for notification.
        :return: List of listener names.
        :rtype: list
        """
        pass

    def get_groups_by_etype(self, etype: str) -> list:
        """
        List all group names from given etype name.

        :param str etype: type of event group (either "incoming" or "outgoing")
        :return: List of group names.
        :rtype: list
        """
        pass

    def get_all_groups(self) -> List[Tuple[str, str]]:
        """
        List all group names in message bus.

        :return: List of group names.
                    In tuple: '(name, etype)'.
                    etype can be either "incoming" or "outgoing", indicating event group type.
        :rtype: list((str,str), ...)
        """
        pass

    # from now on, methods below is for scheudler jobs.

    def start_loop(self):
        pass

    def stop_loop(self, *, wait_timeout=None):
        pass

    def create_event(self, etype: str, source: str,
                     target_groups: Iterable = None, context: dict = None, *,
                     typename: str = "BasicEvent",
                     ignore_not_existed_groups: bool = False) -> None:
        """

        :param str etype: type of event group (either "incoming" or "outgoing")
        :param str source: Indicate where does this event come from. Provided for listeners.
        :param Iterable target_groups: A list of listener group names.
        :param dict context: Context for listeners to receive.
        :param str typename: Custom event type name. Provided for listeners
        :param bool ignore_not_existed_groups: If set to True, will ignore non-existed groups in **target_groups**.
                Default False, will throw **NoSochGroupError** if one of **target_group** is not existed.
        """
        pass


    def _create_new_event(self, *, etype: str, source: str,
                          target_groups: list=None, context: dict=None,
                          typename: str="BasicEvent", token: int=None,
                          ignore_not_existed_groups=False):
        pass

    def _create_stop_signal(self):
        pass

    def _get_eventbus_local_source_name(self):
        return "EventBus Demo:" + self.name

    def _incoming_worker(self):
        pass

    def _outgoing_worker(self):
        pass

    def _event_processor(self, etype: str, event: dict):
        pass


class EventBusError(Exception):
    pass


class NoSuchGroupError(Exception):
    def __init__(self, etype, grpname):
        self.etype=etype
        self.group_name=grpname

    def __str__(self):
        return "Group not found: [" + str(self.etype).upper() + "] " + str(self.group_name)


class NoSuchListenerError(Exception):
    def __init__(self, etype, grpname, listenername):
        self.etype=etype
        self.group_name=grpname
        self.listener_name=listenername

    def __str__(self):
        return "Listener not found: [" + str(self.etype).upper() + "] " + \
               str(self.listener_name) + "@" + str(self.group_name)


class GroupAlreadyExistedError(Exception):
    def __init__(self, etype, grpname):
        self.etype=etype
        self.group_name=grpname

    def __str__(self):
        return "Group already existed: [" + str(self.etype).upper() + "] " + str(self.group_name)


class ListenerAlreadyExistedError(Exception):
    def __init__(self, etype, grpname, listenername):
        self.etype=etype
        self.group_name=grpname
        self.listener_name=listenername

    def __str__(self):
        return "Listener already existed: [" + str(self.etype).upper() + "] " + \
               str(self.listener_name) + "@" + str(self.group_name)