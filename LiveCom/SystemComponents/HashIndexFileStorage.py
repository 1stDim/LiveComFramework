from typing import Union, Optional, IO
from LiveCom.SystemComponents.FilesystemUtils import *
import hashlib
import shutil

base_dir = "Resources"
object_dir = "HashedObjects"

# example hash: 658be3be2d778bf3c62ebf9fa7c786caf4037aaf670f45503e5bd4ecd34f189d

# divide hash:  65 8be3be 2d778bf3c62ebf9f a7c786caf4037aaf 670f45503e5bd4ec d34f189d
# path structs: 65/8be3be/2d778bf3c62ebf9f/a7c786caf4037aaf/670f45503e5bd4ec/d34f189d
#               0          1         2          3         4          5          6
#               12 345678 9012345678901234 5678901234567890 1234567890123456 78901234

def _unified_hash(filehash: str) -> str:
    check_val = filehash.strip().lower()
    if len(check_val) != 64: # check lenth
        raise ValueError("Illegal file hash. Expecting a SHA256 hash string.", filehash)
    try:
        int("0x" + check_val, 0) # check character sets.
    except ValueError:
        raise ValueError("Illegal file hash. Expecting a SHA256 hash string.", filehash)

    return check_val


def _hash_split_to_list(filehash: str):
    uhash = _unified_hash(filehash)
    return [
        uhash[0:2],
        uhash[2:8],
        uhash[8:24],
        uhash[24:40],
        uhash[40:56],
        uhash[56:]
    ]


def get_file_path(filehash: str) -> str:
    pathlist = [base_dir, object_dir]
    pathlist.extend(_hash_split_to_list(filehash))
    return conv_list_to_path(pathlist)
    pass


def get_dir_path_and_file_name(filehash: str) -> (str, str):
    return os.path.split(get_file_path(filehash))
    pass


def create_hash_path(filehash: str) -> None:
    mkdir_recursive(get_dir_path_and_file_name(filehash)[0])
    pass


def calculate_hash(filepath: str, loader_block_size_per_cycle: int=524288) -> str:
    hashval = hashlib.sha256()
    f = open(filepath, mode="rb")
    try:
        while True:
            datablk = f.read(loader_block_size_per_cycle)
            if len(datablk) == 0:
                break
            hashval.update(datablk)
    finally:
        f.close()
    return hashval.hexdigest()
    pass


def upload_file_get_hash(os_filepath: str) -> str:
    # get hash from target file
    hashval = calculate_hash(os_filepath)
    # check if there already a file there
    if _check_hash_target_file_path_already_existed(hashval):
        # already exists, do nothing, just return.
        return hashval
    else:
        # seems this is a new file.
        _copy_file_by_hash(os_filepath, hashval)
    pass

def download_file_from_hash(filehash: str, target_location: str) -> None:
    hash_locatation=get_file_path(filehash)
    if os.path.isfile(hash_locatation):
        if os.path.isdir(target_location):
            shutil.copyfile(hash_locatation, target_location + os.path.pathsep + filehash)
        else:
            shutil.copyfile(hash_locatation, target_location)
    else:
        raise FileNotFoundError("No such hash in storage.", hash_locatation, filehash)
    pass


def _check_hash_target_file_path_already_existed(filehash: str) -> bool:
    path=get_file_path(filehash)
    if os.path.exists(path):
        if os.path.isfile(path):
            return True
        else:
            if os.path.isdir(path):
                raise IsADirectoryError(path)
            else:
                raise FileExistsError("Cannot determine what the heck on this path. \
                                      Are you playing around with the datastore directory??", path)


def _copy_file_by_hash(os_filepath: str, filehash: str) -> None:
    # create target directory first
    create_hash_path(filehash)
    # copy file
    shutil.copyfile(os_filepath, get_file_path(filehash))
    pass


def open_file_by_hash(filehash: str, mode: str = 'rb',
                      buffering: int = -1, encoding: Optional[str] = None,
                      errors: Optional[str] = None, newline: Optional[str] = None,
                      closefd: bool=True, **kwargs) -> IO:
    """
    open_file_by_hash(hash, ...)

    Usage is same as open(), but you need to set *filehash* instead *file* as first argument.
    Default open mode is 'rb' (Read only, binary).

    It is not recommended to open a file for write in this way. Use **upload_file_get_hash()** instead.

    Document for **open()** is listed below:

    -----

    Open file and return a stream.  Raise IOError upon failure.

    file is either a text or byte string giving the name (and the path
    if the file isn't in the current working directory) of the file to
    be opened or an integer file descriptor of the file to be
    wrapped. (If a file descriptor is given, it is closed when the
    returned I/O object is closed, unless closefd is set to False.)

    mode is an optional string that specifies the mode in which the file
    is opened. It defaults to 'r' which means open for reading in text
    mode.  Other common values are 'w' for writing (truncating the file if
    it already exists), 'x' for creating and writing to a new file, and
    'a' for appending (which on some Unix systems, means that all writes
    append to the end of the file regardless of the current seek position).
    In text mode, if encoding is not specified the encoding used is platform
    dependent: locale.getpreferredencoding(False) is called to get the
    current locale encoding. (For reading and writing raw bytes use binary
    mode and leave encoding unspecified.) The available modes are:

    ========= ===============================================================
    Character Meaning
    --------- ---------------------------------------------------------------
    'r'       open for reading (default)
    'w'       open for writing, truncating the file first
    'x'       create a new file and open it for writing
    'a'       open for writing, appending to the end of the file if it exists
    'b'       binary mode
    't'       text mode (default)
    '+'       open a disk file for updating (reading and writing)
    'U'       universal newline mode (deprecated)
    ========= ===============================================================

    The default mode is 'rt' (open for reading text). For binary random
    access, the mode 'w+b' opens and truncates the file to 0 bytes, while
    'r+b' opens the file without truncation. The 'x' mode implies 'w' and
    raises an `FileExistsError` if the file already exists.

    Python distinguishes between files opened in binary and text modes,
    even when the underlying operating system doesn't. Files opened in
    binary mode (appending 'b' to the mode argument) return contents as
    bytes objects without any decoding. In text mode (the default, or when
    't' is appended to the mode argument), the contents of the file are
    returned as strings, the bytes having been first decoded using a
    platform-dependent encoding or using the specified encoding if given.

    'U' mode is deprecated and will raise an exception in future versions
    of Python.  It has no effect in Python 3.  Use newline to control
    universal newlines mode.

    buffering is an optional integer used to set the buffering policy.
    Pass 0 to switch buffering off (only allowed in binary mode), 1 to select
    line buffering (only usable in text mode), and an integer > 1 to indicate
    the size of a fixed-size chunk buffer.  When no buffering argument is
    given, the default buffering policy works as follows:

    * Binary files are buffered in fixed-size chunks; the size of the buffer
      is chosen using a heuristic trying to determine the underlying device's
      "block size" and falling back on `io.DEFAULT_BUFFER_SIZE`.
      On many systems, the buffer will typically be 4096 or 8192 bytes long.

    * "Interactive" text files (files for which isatty() returns True)
      use line buffering.  Other text files use the policy described above
      for binary files.

    encoding is the name of the encoding used to decode or encode the
    file. This should only be used in text mode. The default encoding is
    platform dependent, but any encoding supported by Python can be
    passed.  See the codecs module for the list of supported encodings.

    errors is an optional string that specifies how encoding errors are to
    be handled---this argument should not be used in binary mode. Pass
    'strict' to raise a ValueError exception if there is an encoding error
    (the default of None has the same effect), or pass 'ignore' to ignore
    errors. (Note that ignoring encoding errors can lead to data loss.)
    See the documentation for codecs.register or run 'help(codecs.Codec)'
    for a list of the permitted encoding error strings.

    newline controls how universal newlines works (it only applies to text
    mode). It can be None, '', '\\\\n', '\\\\r', and '\\\\r\\\\n'.  It works as
    follows:

    * On input, if newline is None, universal newlines mode is
      enabled. Lines in the input can end in '\\\\n', '\\\\r', or '\\\\r\\\\n', and
      these are translated into '\\\\n' before being returned to the
      caller. If it is '', universal newline mode is enabled, but line
      endings are returned to the caller untranslated. If it has any of
      the other legal values, input lines are only terminated by the given
      string, and the line ending is returned to the caller untranslated.

    * On output, if newline is None, any '\\\\n' characters written are
      translated to the system default line separator, os.linesep. If
      newline is '' or '\\\\n', no translation takes place. If newline is any
      of the other legal values, any '\\\\n' characters written are translated
      to the given string.

    If closefd is False, the underlying file descriptor will be kept open
    when the file is closed. This does not work when a file name is given
    and must be True in that case.

    A custom opener can be used by passing a callable as *opener*. The
    underlying file descriptor for the file object is then obtained by
    calling *opener* with (*file*, *flags*). *opener* must return an open
    file descriptor (passing os.open as *opener* results in functionality
    similar to passing None).

    open() returns a file object whose type depends on the mode, and
    through which the standard file operations such as reading and writing
    are performed. When open() is used to open a file in a text mode ('w',
    'r', 'wt', 'rt', etc.), it returns a TextIOWrapper. When used to open
    a file in a binary mode, the returned class varies: in read binary
    mode, it returns a BufferedReader; in write binary and append binary
    modes, it returns a BufferedWriter, and in read/write mode, it returns
    a BufferedRandom.

    It is also possible to use a string or bytearray as a file for both
    reading and writing. For strings StringIO can be used like a file
    opened in a text mode, and for bytes a BytesIO can be used like a file
    opened in a binary mode.

    -----

    Internal usage of open_file_by_hash():

    Set keyword argument "_ignore_warning" to True to bypass the file write warning.
    """
    # if file mode is for writing, we should prepare the target path.
    if bool(mode.lower().count("w")) or bool(mode.lower().count("a")) \
            or bool(mode.lower().count("+")) or bool(mode.lower().count("x")):
        create_hash_path(filehash)
        # hidden ignore warning flag chech
        if "_ignore_warning" not in kwargs.keys() or not bool(kwargs["_ignore_warning"]):
            print("WARNING: Attempt writing a file by open_file_by_hash(). File hash is: " + filehash)
    return open(get_file_path(filehash), mode=mode,
                buffering=buffering, encoding=encoding, errors=errors,
                newline=newline, closefd=closefd)
