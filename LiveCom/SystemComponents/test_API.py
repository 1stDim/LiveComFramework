from LiveCom.SystemComponents.EventBus import EventBus
from time import sleep
sysbus = EventBus("API_TestEventBus", system_bus=True)

print("Begin APIManager test.")
# pre-startup
print("before sysbus start")
sysbus.start_loop()

# server-side setup
print("before apihost create")
apihost = sysbus.APIManager.create_library(
    "demolib",
    EventBus.EType.INCOMING,
    "DemoLibServerSide",
    EventBus.EType.OUTGOING,
    "DemoLibClientSide")

print("define worker")
def testlibfunc(val_a, val_b, *, metadata: dict = None):
    print("Called testlibfunc.")
    if metadata is not None:
        print("Source: " + metadata["source"])
    return val_a * val_b

print("before apihost work registration")
apihost.register("testfuncname", testlibfunc) #, metadata_kwarg_name="metadata")

# client-side setup

print("before apiclient create")

apiclient = sysbus.APIManager.load_library("demolib")

print("before apicallermodule create")

demolib = apiclient.spawn_caller_module(False, ignore_api_check=True)

print("before call target")
result = demolib.testfuncname(1, 2)
print("result: " + str(result))

print(str(apiclient.get_function_list(True)))

sysbus.stop_loop()
print("finished APIManager test.")

import gc
print("collected: " + str(gc.collect()))

sysbus = EventBus("API_TestEventBus", system_bus=True)

print("Begin APILoader test.")
from LiveCom.Adaptors.CommonInterfaceSetup import setupCommonGroup
setupCommonGroup(sysbus)
sysbus.start_loop()
sysbus.APIManager.APILoader._system_startup()

sleep(10)

sysbus.APIManager.APILoader._system_shutdown()

print("End of APILoader test.")