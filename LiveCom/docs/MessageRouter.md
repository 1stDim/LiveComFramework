# 消息路由服务

## 总线调度规则
消息路由服务会监听位于消息总线的`INCOMING`方向的`UnifiedMessageProcessors`（统一化消息处理器）这个广播目标上的所有广播消息。而这个广播目标正是所有前端和后端适配器用于回传收到的消息的广播目标。

消息总线会将需要路由的信息发送到`OUTGOING`方向的`UnifiedMessageProcessors`（统一化消息处理器）这个广播目标上。该广播目标被所有前端和后端适配器监听，用于传出消息。

在使用消息路由服务的功能之前，首先应通过API管理器加载路由管理服务的API。API名称为 `MessageRouterControl` 。有关该API下可以使用的API，请参见[API列表](#API列表)。

## 消息路由服务结构与功能

### 路由控制组 (ControlGroup)
路由控制组的作用是方便进行路由上的管理。根据需要，每个适配器可以创建一个或多个控制组。

### 路由链对象 (RouteLink)
路由链对象是具体设定路由的对象。
每个路由对象包括两个组件：
 - 匹配来源
 - 要传送到的目标的列表

## API列表

### 增加路由组
```
add_routing_group(group_name: str) -> None
```
**参数:**

`group_name`: _**str**_: 路由组名称

**异常:** (无)

**返回:** _**None**_: (无)

### 删除路由组
```
del_routing_group(group_name: str) -> None
```
**参数:**

`group_name`: _**str**_: 路由组名称

**异常:**

`NoSuchRoutingGroupError`: 当指定的路由组名称不存在时抛出。

**返回:** _**None**_: (无)

### 设定与更新路由链
```
set_routing_link(group_name: str, link_name: str, source: str, targets: Union[str, Iterable]) -> None
```
**参数:**

`group_name`: _**str**_: 路由组名称
`link_name`: _**str**_: 路由链名称 (用于唯一标记一个路由链)
`source`: _**str**_: 要匹配的消息容器对象名称
`targets`: _**Iterable\[str\]** | **str**_: 要转发到的目标消息容器对象的名称。可以指定为字符串或内容为字符串的列表。


**异常:**

`NoSuchRoutingGroupError`: 当指定的路由组名称不存在时抛出。

**返回:** _**None**_: (无)

### 删除路由链
```
unset_routing_link(group_name: str, link_name: str) -> None
```

**参数:**

`group_name`: _**str**_: 路由组名称
`link_name`: _**str**_: 路由链名称 (用于唯一标记一个路由链)

**异常:**

`NoSuchRoutingGroupError`: 当指定的路由组名称不存在时抛出。
`NoSuchRoutingLinkError`: 当指定的路由链名称不存在时抛出。

**返回:** _**None**_: (无)

### 查询路由组列表
```
get_groups() -> set
```
**参数:** (无)

**异常:** (无)

**返回:** _**set\[str\]**_: 包含已注册的路由组名称的集合。

> 返回值结构:
> - (Return): _**set**_: (返回值)
>   - Values: `group_name`: _**str**_: 路由组名称

### 显示路由组详细数据
```
get_group_context(group_name: str) -> dict
```

**参数:**

`group_name`: _**str**_: 路由组名称


**异常:**

`NoSuchRoutingGroupError`: 当指定的路由组名称不存在时抛出。

**返回:** _**dict**_: 路由组结构字典。

> 返回值结构:
>  - (Return): _**dict**_: (返回值)
>    - Keys: `link_name`: _**str**_: 路由链名称
>    - Values: `link_info`: _**tuple**_: 包含路由链信息的元组
>      - \[0\]: `source`: _**str**_: 所要匹配的消息容器对象名称
>      - \[1\]: `targets`: _**set**_: 所要发到的所有的目标消息容器对象的名称的集合
>        - Values: `target`: _**str**_: 所要发到的单个目标消息容器对象的名称