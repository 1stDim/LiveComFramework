def ModuleInfo():
    return {
        "name": "Services.MessageForwardManager",
        "description": "消息转发管理器",
        "version": "1.0",
        "deprecated": "0.0",
        "require": [
            ("Services.MessageRouter", "0.1"),
            # nothing required.
        ]
    }

def Setup(_sysbus):
    pass

def Takedown(_sysbus):
    pass