from LiveCom.SystemComponents.EventBus import EventBus  # load templates

from LiveCom.Auxiliaries.CQHTTP.cqhttp_msgobj_tools import get_send_target
from time import strftime, localtime

from LiveCom.SystemComponents.API_Demo import _APIBroadcastReceiver, _APIBroadcastConnector, APIManager, _APIHost
from threading import Lock
from typing import Iterable, Union

from LiveCom.Auxiliaries.LiveCom.MessageTargetUtils import conv_container

sysbus = EventBus()

APIHost = _APIHost(sysbus, "", "", "", "", api_manager=APIManager(sysbus))

broadcast_receiver = _APIBroadcastReceiver("","","",api_manager=APIManager(sysbus))
broadcast_connector = _APIBroadcastConnector("","","","" ,api_manager=APIManager(sysbus))

op_lock = Lock()
compile_lock = Lock()
routing_table_version = 0

route_table = {
    # "CQHTTP:G187233857": ["CQHTTP:G688097151", "CQHTTP:G720836502"],
    # "CQHTTP:G688097151": ["CQHTTP:G187233857"],
    # "CQHTTP:G720836502": ["CQHTTP:G187233857"]
}

def onMessage(context, metadata):
    # incoming_context = {
    #     "adaptor": "ADAPTOR.NAME",  # 适配器名称（也就是注册模块时的名称）
    #     "source": "INTERFACE:CONTAINER:USER",  # 标记来源
    #     "message": "inline[type=\"format\", context=\"1\"]of text",  # 消息文本，使用内联消息格式。
    #     "platform_context": {
    #         # 平台相关上下文信息
    #     },
    #     "routing_path": []  # 适配器应当原样转发这个字段，用于路径分析。
    # }

    # 检查目标
    # 如果目标不在被路由的范围，直接跳过。
    msgobj_source = conv_container(context["source"])
    if msgobj_source not in route_table.keys():
        return  # 跳过

    # 确认已有目标。
    # 初步组装目标信息。
    outgoing_context = {
        "target": None,  # 标记目标
        "message": context["message"],  # 消息文本，使用内联消息格式。
    }

    routing_path_current = {
            "adaptor": context["adaptor"],
            "source": context["source"],
            "message": context["message"],
            "target": None,
            "platform_context": context["platform_context"]
    }

    # "routing_path": [  # 消息路由器负责将前一个消息的信息加入这里。[0]始终是当前信息。[1]是前一跳的信息。[-1]是初始消息。
    #     {...}, ...
    # ]
    global broadcast_connector
    for target in route_table[msgobj_source]:
        # 开始组装
        send_context = dict(outgoing_context)
        send_context["target"] = target

        send_path_current = dict(routing_path_current)
        send_path_current["target"] = target
        send_route_path = list()
        send_route_path.append(send_path_current)
        send_route_path.extend(context["routing_path"])

        send_context["routing_path"] = send_route_path
        broadcast_connector(send_context)

        # 完成，发送出去。

    # outgoing_context = {
    #     "target": "INTERFACE:CONTAINER:USER",  # 标记目标
    #     "message": "inline[type=\"format\", context=\"1\"]of text",  # 消息文本，使用内联消息格式。
    #     "routing_path": [  # 消息路由器负责将前一个消息的信息加入这里。[0]始终是当前信息。[1]是前一跳的信息。[-1]是初始消息。
    #         {
    #             "adaptor": "ADAPTOR.NAME",
    #             "source": "INTERFACE:CONTAINER:USER",
    #             "message" "data data data..."
    #             "target": "INTERFACE:CONTAINER:USER",
    #             "platform_context": {
    #                 # 平台相关上下文信息
    #             }
    #         }
    #     ]
    # }






    # get source and process to target name format
    # target_name = get_send_target(context["source"])
    #
    # # lookup actually targets
    # targets_list = None
    # try:
    #     targets_list = route_table[target_name]
    # except KeyError as e:
    #     return  # ignore every route that not defined
    #
    # for target in targets_list:
    #     output = dict(context)
    #     # {
    #     #     "target": context["source"],
    #     #     "source_adaptor": "LOOPBACK",
    #     #     "source": context["source"],
    #     # }
    #
    #     output["target"] = target
    #     output["source_adaptor"] = "MSGROUTER"
    #     output["source"] = context["source"]
    #     # output["message"] = "[" + context["source"] + "@" + \
    #     #                     strftime("%Y-%m-%d %H:%M:%S'", localtime(int(context["time"]))) + \
    #     #                     "]\n" + context["message"]
    #     output["message"] = "[" + str(context["user_id"]) + " " + \
    #                         strftime("%Y-%m-%d %H:%M:%S", localtime(int(context["time"]))) + \
    #                         "]\n" + context["message"]
    #     # sysbus.create_event(sysbus.EType.OUTGOING, context["source"],
    #     #                     ["PlainTextMessageOutgoingProcessors"], output)
    #     global broadcast_connector
    #     broadcast_connector(output)
    # pass


###################################################################
#    Dynamic message router implementation is starting here...    #
###################################################################
routing_group_info = dict()
# routing_group_info = {
#     "GroupName": {
#         "LinkName": ("source", ["targets", "targets"]),
#         "LinkName": ("source", ["targets", "targets"])
#     ]
# }
from LiveCom.Auxiliaries.LiveCom.MessageRouter.MessageRouterExceptions import \
    DuplicatedRoutingGroupError, NoSuchRoutingGroupError, NoSuchRoutingLinkError

def add_routing_group(group_name):
    op_lock.acquire()
    compile_lock.acquire()
    global routing_table_version
    myversion = routing_table_version
    if group_name in routing_group_info.keys():
        raise DuplicatedRoutingGroupError(group_name)
    # create group
    routing_group_info[group_name] = {}
    if myversion == routing_table_version:
        routing_table_version += 1
    compile_lock.release()
    op_lock.release()
    pass

def del_routing_group(group_name):
    op_lock.acquire()
    compile_lock.acquire()
    myversion = routing_table_version
    if group_name not in routing_group_info.keys():
        raise NoSuchRoutingGroupError(group_name)
    routing_group_info.pop(group_name)
    compile_lock.release()
    _compile_routing_table(myversion)
    op_lock.release()
    pass


def set_routing_link(group_name, link_name, source: str, targets: Union[str, Iterable]):
    op_lock.acquire()
    compile_lock.acquire()
    myversion = routing_table_version
    if group_name not in routing_group_info.keys():
        raise NoSuchRoutingGroupError(group_name)

    if isinstance(targets, str):
        tgtsets = {targets}
    else:
        tgtsets = set(targets)
    routing_group_info[group_name][link_name] = (source, tgtsets)
    compile_lock.release()
    _compile_routing_table(myversion)
    op_lock.release()
    pass


def unset_routing_link(group_name, link_name):
    op_lock.acquire()
    compile_lock.acquire()
    myversion = routing_table_version
    if group_name not in routing_group_info.keys():
        raise NoSuchRoutingGroupError(group_name)

    if link_name not in routing_group_info[group_name].keys():
        raise NoSuchRoutingLinkError(group_name, link_name)

    routing_group_info[group_name].pop(link_name)
    compile_lock.release()
    _compile_routing_table(myversion)
    op_lock.release()
    pass


def get_groups():
    op_lock.acquire()
    try:
        return set(routing_group_info.keys())
    finally:
        op_lock.release()
    pass


def get_group_context(group_name):
    ret_list = {}
    op_lock.acquire()
    for link in routing_group_info[group_name].keys():
        ret_list[link] = (routing_group_info[group_name][link][0],
                         routing_group_info[group_name][link][1]
                         )
    op_lock.release()
    return ret_list
    pass


def _compile_routing_table(src_version):
    compile_lock.acquire()
    global routing_table_version
    if not src_version == routing_table_version:
        return

    # compile
    new_route_table = {}
    for group in routing_group_info.keys():
        for link in routing_group_info[group].keys():
            source = routing_group_info[group][link][0]
            target = routing_group_info[group][link][1]
            if source not in new_route_table.keys():
                new_route_table[source] = set()
            new_route_table[source].update(target)
        pass
    global route_table
    route_table = new_route_table
    routing_table_version += 1
    compile_lock.release()
    pass
# def new_routing_worker(context, metadata):
#     pass


###################################################################


def Setup(_sysbus: EventBus):
    global sysbus
    sysbus = _sysbus

    global broadcast_receiver
    global broadcast_connector

    global route_table
    route_table = {}

    broadcast_receiver = sysbus.APIManager.create_broadcast_receiver(
        sysbus.EType.INCOMING,
        "UnifiedMessageProcessors", "Services.MessageRouter")

    broadcast_receiver.register("InternalRouting", onMessage,
                                metadata_kwarg_name="metadata")

    broadcast_connector = sysbus.APIManager.create_brodacast_connecter(
        "MessageRouter", "Services.MessageRouter",
        sysbus.EType.OUTGOING, "UnifiedMessageProcessors",
        allow_invaild_target=True)

    global APIHost
    APIHost = sysbus.APIManager.create_library(
        "MessageRouterControl",
        sysbus.EType.INCOMING, "MsgRouterControlChannel",
        sysbus.EType.OUTGOING, "MsgRouterControlChannel"
    )
    APIHost.register("add_routing_group", add_routing_group)
    APIHost.register("del_routing_group", del_routing_group)
    APIHost.register("set_routing_link", set_routing_link)
    APIHost.register("unset_routing_link", unset_routing_link)
    APIHost.register("get_groups", get_groups)
    APIHost.register("get_group_context", get_group_context)
    global routing_table_version
    _compile_routing_table(0)
    pass

def Takedown(sysbus):
    APIHost._unregister_all()

def ModuleInfo():
    return {
        "name": "Services.MessageRouter",
        "description": "UMP 消息路由服务",
        "version": "0.2",
        "deprecated": "0.0",
        "require": [
            ("LiveComFW", "0.1")
        ]
    }