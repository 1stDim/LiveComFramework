from LiveCom.SystemComponents.EventBus import EventBus
from LiveCom.SystemComponents.API_Demo import APIManager, _APIHost
from LiveCom.Auxiliaries.LiveCom.NameResolver.NameResolverExceptions import NoSuchCodenameError, NoSuchNameError
from LiveCom.Auxiliaries.LiveCom.MessageTargetUtils import conv_dataset

from collections import defaultdict
from typing import Optional, List, Callable


def ModuleInfo():
    return {
        "name": "Services.NameResolver",
        "description": "UMP 名称解析服务",
        "version": "0.1",
        "deprecated": "0.0",
        "require": [
            ("LiveComFW", "0.1")
        ]
    }


_sysbus = EventBus()
APIHost = _APIHost(_sysbus, "", "", "", "", api_manager=APIManager(_sysbus))

resolvers = defaultdict(dict)
load_count = 0
# resolvers = {
#     "codename": {
#         "adaptor_name": {
#             "priority": 1024,
#             "load_seq": 1,
#             "handler": Callable
#         }
#     }
# }


def register_resolver(module_name: str, module_codename: str, handler: Callable, *, priority: int=1024) -> None:
    if not callable(handler):
        raise TypeError("handler should be Callable.")

    if not isinstance(module_name, str):
        raise TypeError("module_name should be str.")

    if not isinstance(module_codename, str):
        raise TypeError("module_codename should be str.")

    if not isinstance(priority, int):
        raise TypeError("priority should be int.")
    global load_count
    resolvers[module_codename][module_name] = {
        "priority": priority,
        "load_seq": load_count,
        "handler": handler
    }
    load_count += 1
    pass


def unregister_resolver(module_name: str, module_codename: Optional[str]=None) -> None:
    if not isinstance(module_name, str):
        raise TypeError("module_name should be str.")

    if not module_codename is str:
        raise TypeError("module_codename should be str.")

    if module_codename is None:
        have_module = False
        for codename in resolvers.keys():
            if module_name in resolvers[codename].keys():
                have_module = True
                resolvers[codename].pop(module_name)
                if len(resolvers[codename]) == 0:  # remove blanks
                    resolvers.pop(codename)

            if not have_module:
                raise NoSuchNameError(None, module_name)
    else:
        if module_codename not in resolvers.keys():
            raise NoSuchCodenameError(module_codename)
        if module_name not in resolvers[module_codename].keys():
            raise NoSuchNameError(module_codename, module_name)
        resolvers[module_codename].pop(module_name)
        if len(resolvers[module_codename]) == 0:  # remove blanks
            resolvers.pop(module_codename)
    pass


def resolve(msgobj: str) -> Optional[tuple]:
    codename, container, user = conv_dataset(msgobj)
    if codename not in resolvers.keys():
        print("Debug: Failed to resolve: No such codename:" + codename)
        return None

    sortdict = dict()
    for info in resolvers[codename]:
        sortdict[(resolvers[codename][info]["priority"], -resolvers[codename][info]["load_seq"])] = resolvers[codename][info]["handler"]

    sequence = list(sortdict.keys())
    sequence.sort(reverse=True)

    for key in sequence:
        remote_retval = sortdict[key](msgobj)
        if remote_retval is not None:
            return remote_retval  # we got it
    print("Debug: Failed to resolve: all providers failed:" + msgobj)
    return None  # all failed.
    pass


def get_module_name_by_codename(codename: str) -> List[str]:
    # search all module in sysbus.
    ret_list = list()
    if "NameResolverUtils" in _sysbus.APIManager.user_extra_data.keys():
        if "AdaptorCodenameTable" in _sysbus.APIManager.user_extra_data["NameResolverUtils"].keys():

            for adaptor in _sysbus.APIManager.user_extra_data["NameResolverUtils"]["AdaptorCodenameTable"]:
                if _sysbus.APIManager.user_extra_data["NameResolverUtils"]["AdaptorCodenameTable"][adaptor] == codename:
                    ret_list.append(adaptor)

    return ret_list
    pass


def Setup(sysbus: EventBus):
    global resolvers
    global load_count
    global APIHost
    global _sysbus
    _sysbus = sysbus
    resolvers = defaultdict(dict)
    load_count = 0

    APIHost = _sysbus.APIManager.create_library("NameResolver",
                                                _sysbus.EType.INCOMING,
                                                "UMPNameServiceAPI",
                                                _sysbus.EType.OUTGOING,
                                                "UMPNameServiceAPI")
    APIHost.register("register_resolver", register_resolver)
    APIHost.register("unregister_resolver", unregister_resolver)
    APIHost.register("resolve", resolve)
    APIHost.register("get_module_name_by_codename", get_module_name_by_codename)

    pass


def Takedown(sysbus):
    APIHost._unregister_all()
    pass

