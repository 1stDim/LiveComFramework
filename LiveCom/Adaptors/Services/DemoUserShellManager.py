# DemoUserShellManager

from LiveCom.SystemComponents.EventBus import EventBus
from LiveCom.Auxiliaries.LiveCom.MessageTargetUtils import conv_container
from LiveCom.SystemComponents.API_Demo import _APIBroadcastReceiver, _APIBroadcastConnector, APIManager, APICallerModule, _APIClient
from shlex import split as cmd_split
import LiveCom.Auxiliaries.LiveCom.SessionManager.SessionUtilsLibTemplates as SessionManagerUtils
from LiveCom.Auxiliaries.LiveCom.SessionManager.SessionManagerExceptions import *
from LiveCom.Auxiliaries.LiveCom.SystemInfo import getSystemVersion, getCopyrightInfo

incoming_receiver = _APIBroadcastReceiver("","","", api_manager=APIManager(EventBus()))
outgoing_sender = _APIBroadcastConnector("", "", "", "", api_manager=APIManager(EventBus()))

SessionManagerUtilsClient = _APIClient(EventBus(), "", "", "", "", "", libname="", api_manager=APIManager(EventBus()))
# SessionManagerUtils = SessionManagerUtilsClient.spawn_caller_module()

from threading import Lock
msg_op_lock = Lock()

debug_enabled=True

def ModuleInfo():
    return {
        "name": "Services.DemoUserShellManager",
        "description": "演示用会话管理服务前端",
        "version": "0.1",
        "deprecated": "0.0",
        "require": [
            ("LiveComFW", "0.1"),
            ("Services.SessionManager", "0.1")
        ]
    }

from pprint import pprint

def proc_input(context, metadata):
    if debug_enabled:
        if not metadata["source"] == "Services.MessageRouter":
            msg = context["message"]
            src = context["source"]
            adaptor = context["adaptor"]
            if adaptor.split(".", maxsplit=1)[0] == "Frontend":
                # we got input.
                if msg[0:2] == "~:":
                    pass # we got command
                    cmdline = msg.split(":", maxsplit=1)[1]
                    proc_cmdline(src, cmdline)

    return  # everything else is not our business.

def proc_cmdline(src: str, cmdline: str):
    argv = cmd_split(cmdline)
    if argv[0].lower() == "help":
        send_msg(conv_container(src), """\
LiveCom 会话管理

~:help              \t 显示此帮助
~:list [Backend]    \t 获取应用列表
~:start [AppID]     \t 启动新的用户会话
~:version           \t 显示版本信息""")
        pass
    elif argv[0].lower() == "start":
        app_id = None
        if len(argv) > 2:
            send_msg(conv_container(src), "LiveCom: 参数太多。 输入 '~:help' 以获得详细帮助。")
        if len(argv) == 1:
            app_id = None
        else:
            app_id = argv[1]

        send_msg(conv_container(src), "LiveCom: 正在创建用户会话。")
        try:
            SessionManagerUtils.CreateSession(conv_container(src), app_id)
        except NoSuchBackendCodenameError:
            send_msg(conv_container(src), "LiveCom: 未能启动会话: 指定的服务后端不存在。")
        except NoSuchAppError:
            send_msg(conv_container(src), "LiveCom: 未能启动会话: 指定的应用不存在。")
        pass
    elif argv[0].lower() == "list":
        backend_name = None
        if len(argv) > 2:
            send_msg(conv_container(src), "LiveCom: 参数太多。 输入 '~:help' 以获得详细帮助。")
        if len(argv) == 1:
            backend_name = None
        else:
            backend_name = argv[1]

        result = []
        try:
            result = SessionManagerUtils.ListApps(backend_name)
        except NoSuchBackendCodenameError:
            send_msg(conv_container(src), "LiveCom: 查找应用失败: 指定的服务后端不存在。")

        if len(result) == 0:
            send_msg(conv_container(src), "LiveCom: 查找应用失败: 未找到任何应用。")
            return

        namelist = ""
        for appname, appdesc in result:
            namelist += appname + ": " + appdesc + "\n"

        namelist = namelist.rstrip("\n")
        send_msg(conv_container(src),
                 "LiveCom: 应用列表: \n\n" + namelist)

    elif argv[0].lower() == "version":
        send_msg(conv_container(src), getCopyrightInfo())
        pass
    elif argv[0].lower() == "lblive" or \
            argv[0].lower().replace("-", "").replace("_", "") == "wearebusters":
        # we have something to say. -- SuperMarioSF
        from LiveCom.Auxiliaries.LiveCom.SystemInfo import _LetsRememberThatOldDays as _ThankYou
        send_msg(conv_container(src), _ThankYou())
    #     pass
    # elif argv[0].lower() == "help":
    #     pass
    # elif argv[0].lower() == "help":
    #     pass
    else:
        send_msg(conv_container(src), "LiveCom：无效的会话管理命令。 输入 '~:help' 以获得详细帮助。")


def send_msg(target_msgobj: str, text: str):
    outgoing_sender({
        "target": target_msgobj,  # 标记目标
        "message": text,  # 消息文本，使用内联消息格式。
        "routing_path": [  # 消息路由器负责将前一个消息的信息加入这里。[0]始终是当前信息。[1]是前一跳的信息。[-1]是初始消息。
            {
                "adaptor": "Services.DemoUserShellManager",
                "source": "DEBUGSHELLMGR:DUMMY:DUMMY",
                "message": text,
                "target": target_msgobj,
                "platform_context": {}
            }
        ]
    })
    pass

def Takedown(sysbus: EventBus):
    incoming_receiver.unregister("Receiver")
    pass

def Setup(sysbus: EventBus):
    global incoming_receiver
    global outgoing_sender
    global _sysbus
    global SessionManagerUtilsClient
    global SessionManagerUtils

    _sysbus = sysbus

    SessionManagerUtilsClient = sysbus.APIManager.load_library("SessionUtilsLib")
    SessionManagerUtils = SessionManagerUtilsClient.spawn_caller_module()

    incoming_receiver = sysbus.APIManager.create_broadcast_receiver(EventBus.EType.INCOMING,
                                                                    "UnifiedMessageProcessors", "DebugUserShellManager")

    outgoing_sender = sysbus.APIManager.create_brodacast_connecter("DebugUserShellManager",
                                                                   "Services.DebugUserShellManager",
                                                                   EventBus.EType.OUTGOING,
                                                                   "UnifiedMessageProcessors")
    incoming_receiver.register("Receiver", proc_input,
                               metadata_kwarg_name="metadata")

    pass