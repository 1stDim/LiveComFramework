from collections import defaultdict

from LiveCom.SystemComponents.EventBus import EventBus
from LiveCom.SystemComponents.API_Demo import APIManager as _APIManager, _APIHost, _APIClient
from LiveCom.Auxiliaries.LiveCom.SessionManager.SessionManagerExceptions import *
# from collections import defaultdict
from LiveCom.Auxiliaries.LiveCom.SessionManager.SessionManagerUtils import *
from LiveCom.Config.LoadConfig import load_config

from LiveCom.Auxiliaries.LiveCom.MessageTargetUtils import conv_dataset
from typing import Union, Optional, Iterable, List, Tuple

import LiveCom.Auxiliaries.LiveCom.MessageRouter.MessageRouterCallerTemplate as _routercil

# 公用服务对象
DEFAULT_SESSION_APPID = ""  # read from config
SESSMGR_NAME = "SessionManager"

sysbus = EventBus()
# APIManager = _APIManager(sysbus)

SMBLib = None #_APIHost(sysbus, "","","","",libname="",api_manager=APIManager(sysbus))
SessionUtilsLib = None # _APIHost(sysbus, "","","","",libname="",api_manager=APIManager(sysbus))

RouterCtl = _routercil
RouteLib = None

# 内部公共对象与处理函数

backend_list = dict()
sessions = dict()


# SessionManager 内部调用函数
def get_next_available_session_id() -> int:
    start = 0
    end   = 0xFFFFFFFF # Max 2^8 sessions.
    for i in range(start, end + 1):
        if i not in sessions.keys():
            return i
    # seems no available id.
    raise ValueError("No more available session ID.")
    pass


def decode_session_name(hexstring: str) -> int:
    return int(hexstring, base=16)
    pass


def create_session(session_id: int, terminal_name, app_name: str=None):
    # we have session_id, terminal_name, app_name(might be blank?)
    # what app should we start?
    print("会话管理器: 创建新会话: " + generate_hexstring(session_id))
    default_app = False
    if app_name is None:
        app_name = DEFAULT_SESSION_APPID
        default_app = True
    # get default structure ready.
    sessions[session_id] = {
        "UserTerminal": terminal_name,
        "Processes": {}
    }
    # create default process
    if default_app:
        print("会话管理器: 创建新会话: " + generate_hexstring(session_id, 8) + ": 加载默认应用")
    else:
        print("会话管理器: 创建新会话: " + generate_hexstring(session_id, 8) + ": 加载应用: " + app_name)
    terminal_id = open_process(session_id, app_name, None)

    # start target task
    if default_app:
        print("会话管理器: 创建新会话: " + generate_hexstring(session_id, 8) + ": 启动默认应用")
    else:
        print("会话管理器: 创建新会话: " + generate_hexstring(session_id, 8) + ": 启动应用: " + app_name)
    start_task(session_id, terminal_id)
    pass


# def destory_session(session_name):
#     pass


def open_process(session_id: int, app_name: str, arg_info=None):
    if session_id not in sessions.keys():
        raise NoSuchSessionError(session_id)

    backend_codename = app_name.split("/", maxsplit=1)[0]
    if backend_codename not in backend_list.keys():
        raise NoSuchBackendCodenameError(backend_codename)

    if not len(app_name.split("/", maxsplit=1)) == 2:
        raise NoSuchAppError("<Not Provided>")

    # call api client for target function.
    print("会话管理器: 加载应用: " + app_name + " 位于会话 " + generate_hexstring(session_id))
    terminal_id = backend_list[backend_codename]["libclient"].call_api("OpenApp", tuple(), {
        "session_id": session_id,
        "app_name": app_name,
        "arg_info": arg_info
    })

    # write TID to session info.
    # sessions[session_id] = {
    #     "UserTerminal": terminal_name,
    #     "Processes": {}
    # }
    sessions[session_id]["Processes"][terminal_id] = {
        "ProcessLock": None,
        "StatGetLock": Lock(),  # used to block outer getter function
        "StatGetWaiter": ZeroWaiter(),  # used to block process info removal
        "ReturnVal": None
    }
    return terminal_id
    pass


def start_task(session_id: int, terminal_id: str, wait: Optional[Union[bool, int]]=False):
    if session_id not in sessions.keys():
        raise NoSuchSessionError(session_id)

    if terminal_id not in sessions[session_id]["Processes"].keys():
        raise NoSuchTerminalError(session_id, terminal_id)

    # from terminal_id get backend.
    print("会话管理器: 启动任务: 进程终端 " + terminal_id)
    backend_codename, terminal, dummy = conv_dataset(terminal_id)
    if backend_codename not in backend_list.keys():
        raise NoSuchBackendCodenameError(backend_codename)


    # TODO: Multipath IO setup
    print("会话管理器: 启动任务: 进程终端 " + terminal_id + ": 设定路由")
    RouterCtl.set_routing_link(
        SESSMGR_NAME,
        "Session(" + generate_hexstring(session_id) + ").Terminal(" + terminal_id + ")-I",
        sessions[session_id]["UserTerminal"],
        [terminal_id]
    )

    RouterCtl.set_routing_link(
        SESSMGR_NAME,
        "Session(" + generate_hexstring(session_id) + ").Terminal(" + terminal_id + ")-O",
        terminal_id,
        [sessions[session_id]["UserTerminal"]]
    )

    # start process
    print("会话管理器: 启动任务: 进程终端 " + terminal_id + ": 启动进程")
    proc_lock = backend_list[backend_codename]["libclient"].call_api(
        "StartApp",
        (terminal_id, ),
        dict()
    )
    if not proc_lock.locked():
        # someone forgot to lock?
        proc_lock.acquire()
    # write proc lock
    sessions[session_id]["Processes"][terminal_id]["ProcessLock"] = proc_lock

    print("会话管理器: 启动任务: 进程终端 " + terminal_id + ": 完成")
    if wait is not None and not wait and not wait == 0:
        return

    if wait is None:
        wait = -1

    if wait is False:
        return False

    return wait_for_return(session_id, terminal_id, timeout=wait)
    pass


def wait_for_return(session_id: int, terminal_id: str, timeout: int=-1):
    if session_id not in sessions.keys():
        raise NoSuchSessionError(session_id)

    if terminal_id not in sessions[session_id]["Processes"].keys():
        raise NoSuchTerminalError(session_id, terminal_id)


    rem = sessions[session_id]["Processes"][terminal_id]["StatGetWaiter"].add_count()
    print("会话管理器: 等待运行结果: 进程终端 " + terminal_id + ": 创建新等待事务 (队列中有 " + str(rem) + "个事务正在等待)")
    stat = False
    try:
        # wait here
        stat = sessions[session_id]["Processes"][terminal_id]["ProcessLock"].acquire(timeout=timeout)
        return sessions[session_id]["Processes"][terminal_id]["ReturnVal"]
    finally:
        if stat:
            sessions[session_id]["Processes"][terminal_id]["ProcessLock"].release()
        rem = sessions[session_id]["Processes"][terminal_id]["StatGetWaiter"].del_count()
        if stat:
            print("会话管理器: 等待运行结果: 进程终端 " + terminal_id + ": 已取得返回值 (队列中有 " + str(rem) + "个事务正在等待)")
        else:
            print("会话管理器: 等待运行结果: 进程终端 " + terminal_id + ": 等待超时或放弃等待 (队列中有 " + str(rem) + "个事务正在等待)")
    pass


def stop_task(session_id: int, terminal_id: str, force: bool=False):
    # find source adaptor
    backend_codename, terminal, dummy = conv_dataset(terminal_id)
    if backend_codename not in backend_list.keys():
        raise NoSuchBackendCodenameError(backend_codename)

    if force:
        print(
            "会话管理器: 结束进程: 会话 " + generate_hexstring(session_id) +
            " 进程终端 " + terminal_id + ": 强制结束")
    else:
        print(
            "会话管理器: 结束进程: 会话 " + generate_hexstring(session_id) +
            " 进程终端 " + terminal_id + ": 结束任务")

    backend_list[backend_codename]["libclient"].call_api(
        "StopApp",
        (session_id, terminal_id, force),
        {}
    )
    pass


# def list_task():
#     pass
#
#
# def list_session():
#     pass

# TODO: WIP
# def move_session():
#     pass


# SessionManager对外公开函数
# 第一组: 后端服务接口 （"SessionManagerBackendUtils", "SMBLib"）
def SMBLib_RegisterBackend(backend_codename: str, backend_libname: str):
    # load target library
    if backend_codename in backend_list.keys():
        raise DuplicatedBackendCodenameError(backend_codename, backend_libname)

    # get APIClient
    apiclient = sysbus.APIManager.load_library(backend_libname)

    # create info structure
    # WARNING: target library must be available this time.
    backend_list[backend_codename] = {
        "libname": backend_libname,
        "libclient": apiclient,
        "app_list": {}  # app_list = { "APP_Name": "Description" }
    }
    print(
        "会话管理器: 服务连接: 适配器代号 " + backend_codename + " 已连接。支持库名称: " +
        backend_libname
    )
    # setup done.
    pass


def SMBLib_UnregisterBackend(backend_codename: str):
    if backend_codename not in backend_list.keys():
        raise NoSuchBackendCodenameError(backend_codename)

    # call shutdown to let backend do cleanup.
    # we will wait for complete.
    print("会话管理器: 反注册后端: 正在反注册 " + backend_codename)
    backend_list[backend_codename]["libclient"].call("Shutdown", tuple(), dict())
    # returned, then do cleanup.
    backend_list.pop(backend_codename)
    pass


def SMBLib_UpdateAppListEvent(backend_codename: str, new_app_list: dict):
    # use this new shiny list to replace current haveing list for that backend.
    if backend_codename not in backend_list:
        raise NoSuchBackendCodenameError(backend_codename)
    print("会话管理器: 更新应用列表: 适配器代号 " + backend_codename + " 已更新应用列表。")
    backend_list[backend_codename]["app_list"] = new_app_list
    pass


def SMBLib_AppExitEvent(session_id: int, terminal_id: str, returnval):
    if session_id not in sessions.keys():
        raise NoSuchSessionError(session_id)

    if terminal_id not in sessions[session_id]["Processes"].keys():
        raise NoSuchTerminalError(session_id, terminal_id)

    print(
        "会话管理器: 进程退出: 进程终端 " + terminal_id + " 位于会话 " +
        generate_hexstring(session_id) + ": 解除设定路由"
    )
    # TODO: Multipath IO removal
    RouterCtl.unset_routing_link(
        SESSMGR_NAME,
        "Session(" + generate_hexstring(session_id) + ").Terminal(" + terminal_id + ")-I"
    )

    RouterCtl.unset_routing_link(
        SESSMGR_NAME,
        "Session(" + generate_hexstring(session_id) + ").Terminal(" + terminal_id + ")-O"
    )

    print(
        "会话管理器: 进程退出: 进程终端 " + terminal_id + " 位于会话 " +
        generate_hexstring(session_id) + ": 取得返回值"
    )
    # get return value
    sessions[session_id]["Processes"][terminal_id]["ReturnVal"] = returnval
    print(
        "会话管理器: 进程退出: 进程终端 " + terminal_id + " 位于会话 " +
        generate_hexstring(session_id) + ": 通知返回值取得事件"
    )
    sessions[session_id]["Processes"][terminal_id]["ProcessLock"].release()
    print(
        "会话管理器: 进程退出: 进程终端 " + terminal_id + " 位于会话 " +
        generate_hexstring(session_id) + ": 等待全部返回值取得会话完成处理"
    )
    # wait for returnval getter finish their tasks.
    sessions[session_id]["Processes"][terminal_id]["StatGetWaiter"].wait_zero()
    print(
        "会话管理器: 进程退出: 进程终端 " + terminal_id + " 位于会话 " +
        generate_hexstring(session_id) + ": 销毁进程"
    )
    # destory process info
    sessions[session_id]["Processes"].pop(terminal_id)

    # check if session does not have any processes
    if len(sessions[session_id]["Processes"]) == 0:
        # destory session info
        print(
            "会话管理器: 销毁会话: 会话 " +
            generate_hexstring(session_id) + ": 会话内所有进程已退出，销毁会话"
        )
        sessions.pop(session_id)
    pass


# 第二组: 应用程序服务接口 ("SessionManagerCtl", "SessionUtilsLib")


def CreateSession(user_terminal_target: str, app_name: str=None):
    # we should get a available session id first.
    try:
        next_id = get_next_available_session_id()
    except ValueError as e:
        raise RuntimeError("No more sessions available.") from e
    create_session(next_id, user_terminal_target, app_name)
    return generate_hexstring(next_id, 8)
    pass


def CreateProcess(session_name: str, app_name: str, arg_info: dict=None):
    session_id = decode_session_name(session_name)
    if session_id not in sessions.keys():
        raise NoSuchSessionError(session_id)

    # open process
    terminal_id = open_process(session_id, app_name, arg_info)
    # start task
    start_task(session_id, terminal_id, wait=False)
    return terminal_id
    pass


def CreateProcessAndWaitForReturn(session_name: str, app_name: str, arg_info: dict=None, timeout: int=-1):
    session_id = decode_session_name(session_name)
    if session_id not in sessions.keys():
        raise NoSuchSessionError(session_id)

    # open process
    terminal_id = open_process(session_id, app_name, arg_info)
    # start task
    ret = start_task(session_id, terminal_id, wait=timeout)
    return ret
    pass


def WaitForReturnValue(session_name: str, terminal_id: str, timeout: int=-1):
    session_id = decode_session_name(session_name)
    if session_id not in sessions.keys():
        raise NoSuchSessionError(session_id)

    return wait_for_return(session_id, terminal_id, timeout)
    pass


def DestorySession(session_name: str):
    if decode_session_name(session_name) not in sessions.keys():
        raise NoSuchSessionError(decode_session_name(session_name))

    # kill all process under that session.
    for process in sessions[decode_session_name(session_name)]["Processes"].keys():
        # process name indicate it backend adaptor.
        stop_task(decode_session_name(session_name), process, True)
    pass


def DestoryProcess(session_name: Optional[str], terminal_id: str, force=False):
    pendinging_search_sessions = []
    if session_name is not None:
        # check session availablity
        if decode_session_name(session_name) not in sessions.keys():
            raise NoSuchSessionError(decode_session_name(session_name))
        pendinging_search_sessions.append(decode_session_name(session_name))
    else:
        # get all sessions
        pendinging_search_sessions.extend(sessions.keys())

    take_actions = []
    for src_session in pendinging_search_sessions:
        if terminal_id in sessions[src_session]["Processes"].keys():
            take_actions.append((src_session, terminal_id))

    if len(take_actions) == 0:
        raise NoClosableProcessAvailableError(terminal_id)

    for session, terminal_id in take_actions:
        stop_task(session, terminal_id, force)

    pass


def ListTask(backend_codename: Optional[Union[str, Iterable]]=None,
             session_name: Optional[Union[str, Iterable]]=None) -> List[Tuple[str]]:
    backends = []

    if backend_codename is None:
        for backend in backend_list.keys():
            backends.append(backend)
    elif isinstance(backend_codename, str):
        if backend_codename not in backend_list.keys():
            raise NoSuchBackendCodenameError(backend_codename)
        backends.append(backend_codename)
    elif isinstance(backend_codename, Iterable):
        for backend in backend_codename:
            if backend not in backend_list.keys():
                raise NoSuchBackendCodenameError(backend)
            backends.append(backend)
    else:
        raise TypeError("backend_codename should be None, str or Iterable.")

    # got backend to be checked.
    # we only accept one session to be checked.
    session_to_be_check = []
    if isinstance(session_name, Iterable):
        for session in session_name:
            if decode_session_name(session) not in sessions.keys():
                raise NoSuchSessionError(decode_session_name(session))
            session_to_be_check.append(decode_session_name(session))
    elif isinstance(session_name, str):
        if decode_session_name(session_name) not in sessions.keys():
            raise NoSuchSessionError(decode_session_name(session_name))
        session_to_be_check.append(session_name)

    elif session_name is None:
        session_to_be_check = None

    print("会话管理器: 状态查询: 正在查询运行中的应用")
    ret_val = []
    for backend in backends:
        context = backend_list[backend]["libclient"].call_api("ListOpenedApp",
                                                            (session_to_be_check, ),
                                                            {})
        ret_val.extend(context)

    return ret_val
    pass


def ListSession():
    ret_val = defaultdict(list)
    for session in sessions.keys():
        for process in sessions[session]["Processes"].keys():
            ret_val[generate_hexstring(session, 8)].append(process)

    return dict(ret_val)
    pass


def ListApps(backend_codename: str=None):
    # actively refresh, so no need for do refresh here.
    ret_val = []
    pending_check = []
    if backend_codename is None:
        # show all apps
        pending_check = backend_list.keys()
    else:
        if backend_codename not in backend_list.keys():
            raise NoSuchBackendCodenameError(backend_codename)
        pending_check.append(backend_codename)

    for backend in pending_check:
        app_list = backend_list[backend]["app_list"]
        for appname in app_list.keys():
            ret_val.append(
                (
                    backend + "/" + appname,
                    app_list[appname]  # description
                )
            )
    return ret_val
    pass


# 服务适配器注册

def Setup(_sysbus: EventBus):
    global backend_list
    global sysbus
    #global APIManager
    global sessions
    global DEFAULT_SESSION_APPID
    sysbus = _sysbus
    print("会话管理器: 初始化: 加载API管理器")
    backend_list = dict()
    sessions = dict() # defaultdict(dict)
    print("会话管理器: 初始化: 读取配置文件")
    # 加载配置文件
    config = load_config("SessionManager.Config")
    DEFAULT_SESSION_APPID = config["default_session_app"]
    print("会话管理器: 初始化: 加载路由控制服务API")
    # 加载路由控制服务
    global RouterCtl
    global RouteLib
    RouterLib = sysbus.APIManager.load_library("MessageRouterControl")
    RouterCtl = RouterLib.spawn_caller_module()

    print("会话管理器: 初始化: 建立会话管理器路由组")
    # 创建初始路由组
    RouterCtl.add_routing_group(SESSMGR_NAME)

    print("会话管理器: 初始化: 注册会话管理器后端工具库API")
    global SMBLib
    SMBLib = _sysbus.APIManager.create_library("SMBLib", _sysbus.EType.INCOMING, "SessionManagerSMBLibCtl", _sysbus.EType.OUTGOING, "SessionManagerSMBLibCtl")
    SMBLib.register("RegisterBackend", SMBLib_RegisterBackend)
    SMBLib.register("UnregisterBackend", SMBLib_UnregisterBackend)
    SMBLib.register("UpdateAppListEvent", SMBLib_UpdateAppListEvent)
    SMBLib.register("AppExitEvent", SMBLib_AppExitEvent)

    print("会话管理器: 初始化: 注册会话管理器API")
    global SessionUtilsLib
    SessionUtilsLib = sysbus.APIManager.create_library("SessionUtilsLib", sysbus.EType.INCOMING, "SessionManagerControl", sysbus.EType.OUTGOING, "SessionManagerControl")
    SessionUtilsLib.register("CreateSession", CreateSession)
    SessionUtilsLib.register("CreateProcess", CreateProcess)
    SessionUtilsLib.register("CreateProcessAndWaitForReturn", CreateProcessAndWaitForReturn)
    SessionUtilsLib.register("WaitForReturnValue", WaitForReturnValue)
    SessionUtilsLib.register("DestorySession", DestorySession)
    SessionUtilsLib.register("DestoryProcess", DestoryProcess)
    SessionUtilsLib.register("ListTask", ListTask)
    SessionUtilsLib.register("ListSession", ListSession)
    SessionUtilsLib.register("ListApps", ListApps)

    print("会话管理器: 初始化: 已完成")
    pass


def Takedown(sysbus):
    print("会话管理器: 反初始化: 反注册会话管理器后端工具库API")
    SMBLib._unregister_all()

    print("会话管理器: 反初始化: 反注册会话管理器API")
    SessionUtilsLib._unregister_all()

    print("会话管理器: 反初始化: 正在反注册后端")
    for backend in backend_list.keys():
        SMBLib_UnregisterBackend(backend)
    print("会话管理器: 反初始化: 已完成")
    pass


def ModuleInfo():
    return {
        "name": "Services.SessionManager",
        "description": "用户会话管理器",
        "version": "0.1",
        "deprecated": "0.0",
        "require": [
            ("LiveComFW", "0.1"),
            ("Services.MessageRouter", "0.1")
        ]
    }

def __del__():
    print("会话管理器: 反初始化: 资源正在回收")