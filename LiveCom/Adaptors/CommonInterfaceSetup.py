"""
Common interface setup
This file is used to create most common group interface to use.
"""
from LiveCom.SystemComponents.EventBus import EventBus

def setupCommonGroup(sysbus: EventBus):
    sysbus.create_group(sysbus.EType.INCOMING, "PlainTextMessageIncomingProcessors")
    sysbus.create_group(sysbus.EType.OUTGOING, "PlainTextMessageOutgoingProcessors")