from LiveCom.SystemComponents.EventBus import EventBus
from LiveCom.Auxiliaries.CQHTTP.cqhttp_helper import *
from threading import Thread
from LiveCom.Auxiliaries.CQHTTP.cqhttp_msgobj_tools import msgobj_generator, msgobj_paraser
from time import strftime, localtime, sleep
from LiveCom.Auxiliaries.LiveCom.NameResolver.NameResolverCallerTemplate import *
from LiveCom.Auxiliaries.LiveCom.NameResolver.NameResolverExceptions import *
from LiveCom.Auxiliaries.CQHTTP.cqhttp_msgobj_tools import msgobj_paraser
from LiveCom.Config.LoadConfig import load_config

from LiveCom.SystemComponents.API_Demo import _APIClient, APIManager, APICallerModule

MODULE_NAME = "Frontend.CQHTTP"
bot_worker = None  # thread worker object
cache_worker = None  # group list cache worker object
cfgobject = dict()

from LiveCom.Auxiliaries.LiveCom.API.APIToolkits import APIUMPModuleUtils

# 由于CQHTTP PythonSDK的设计原因，必须先初始化CQHTTP对象。（？）
bot = None

# 系统总线暂时留空，在设置时生效。
sysbus = EventBus()
UMP = None #APIUMPModuleUtils
local_group_cache = list()
cache_worker_enabled = False

broadcast_sender = None  # _APIBroadcastConnector()
broadcast_receiver = None  #_APIBroadcastReceiver()
remote_resolver_lib = None # _APIClient(sysbus, "", "", "", "", "", libname="", api_manager=APIManager(sysbus))
r_resolve = None  #APICallerModule(remote_resolver_lib)

def incoming_receiver(context):
    # # 处理消息来源信息作为来源类型信息
    # srcname = msgobj_generator(context)
    #
    # # context信息复制后增加来源信息后作为标准信息数据。
    context_send = dict(context)
    context_send["source"] = msgobj_generator(context)
    context_send["source_adaptor"] = "CQHTTP"
    # # msg为信息体，作为纯文本数据发出。
    # # 发送数据

    # 使用新的消息传送标准重写。
    sending_context = {
        "adaptor": MODULE_NAME,  # 适配器名称（也就是注册模块时的名称）
        "source": msgobj_generator(context),  # 标记来源
        "message": context["message"],  # 消息文本，使用内联消息格式。
        "platform_context": context_send,
        "routing_path": []  # 源输入适配器此处可以留空。
    }




    global broadcast_sender
    broadcast_sender(sending_context)

    # sysbus.create_event(etype=sysbus.EType.INCOMING, source=srcname,
    #                     target_groups=["PlainTextMessageIncomingProcessors"],
    #                     context=context_send, ignore_not_existed_groups=True)


def outgoing_sender(context, metadata):
    global r_resolve
    # 使用新的消息传送标准重写
    # outgoing_context = {
    #     "target": "INTERFACE:CONTAINER:USER",  # 标记目标
    #     "message": "inline[type=\"format\", context=\"1\"]of text",  # 消息文本，使用内联消息格式。
    #     "routing_path": [  # 消息路由器负责将前一个消息的信息加入这里。[0]始终是当前信息。[1]是前一跳的信息。[-1]是初始消息。
    #         {
    #             "adaptor": "ADAPTOR.NAME",
    #             "source": "INTERFACE:CONTAINER:USER",
    #             "message" "data data data..."
    #             "target": "INTERFACE:CONTAINER:USER",
    #             "platform_context": {
    #                 # 平台相关上下文信息
    #             }
    #         }
    #     ]
    # }
    tgtname = context["target"]
    tgtinfo = msgobj_paraser(tgtname)
    if tgtinfo is None:
        return  # 不是CQHTTP为目标的信息，不处理。

    # 处理要发送的信息
    msg_send = ""
    if metadata["source"] == "Services.MessageRouter":  # from message router
        if context["routing_path"][0]["adaptor"].split(".", maxsplit=1)[0] == "Backend":
            pass
        else:
            # TODO: 此处应有名称解析。
            result = r_resolve.resolve(context["routing_path"][0]["source"])
            if result is not None:
                platform, container, username = result
                if username is None:
                    display_name = platform + ": " + container
                else:
                    if cfgobject["no-container-names"]:
                        display_name = platform + ": " + username
                    else:
                        display_name = platform + ": " + container + "/" + username
            else:
                display_name = context["routing_path"][0]["source"]

            msg_send += "[" + display_name + " " + \
                                strftime("%H:%M:%S", localtime(int(
                                    context["routing_path"][0]["platform_context"]["time"]
                                ))) + "]\n"

        pass

    msg_send += context["message"]


    # print("SendOut: \n\tcontext=" + str(context)+ "\n\ttgtinfo=" + str(tgtinfo))
    # context["message"] += "\ntgtinfo=" + str(tgtinfo) + "\ncontext=" + str(context)
    if tgtinfo["message_type"] == "private":
        pass
        bot.send_private_msg(user_id=tgtinfo["user_id"], message=msg_send)
    elif tgtinfo["message_type"] == "group":
        pass
        bot.send_group_msg(group_id=tgtinfo["group_id"], message=msg_send)
    elif tgtinfo["message_type"] == "discuss":
        pass
        bot.send_discuss_msg(discuss_id=tgtinfo["discuss_id"], message=msg_send)



    pass


def name_resolver(msgobj: str):
    global local_group_cache
    userinfo = msgobj_paraser(msgobj)
    platform = "QQ"
    container = None
    user = None
    partial = userinfo["partial"]
    if userinfo["message_type"] == "private":
        container = "<私聊消息>"
        if "sub_type" in userinfo.keys():
            if userinfo["sub_type"] == "friend":
                container = "<好友私聊>"
            if userinfo["sub_type"] == "group":
                container = "<群组私聊>"
            if userinfo["sub_type"] == "discuss":
                container = "<讨论组私聊>"
            if userinfo["sub_type"] == "other":
                container = "<其他私聊>"
        # user info get
        try:
            # print("get user info: cache")
            uid_info = bot.get_stranger_info(user_id=userinfo["user_id"], no_cache=False)
        except Error:
            try:
                print("CQHTTP: 名称解析: 警告: 缓存查询失败，正尝试无缓存查询: UID=" + userinfo["user_id"])
                uid_info = bot.get_stranger_info(user_id=userinfo["user_id"], no_cache=True)  # refresh cache
            except Error as err:
                print("CQHTTP: 名称解析: 错误: 查询用户信息失败:  UID=" + userinfo["user_id"] + ", 状态码=" + str((err.status_code, err.ret_code)))
                uid_info = None
        if uid_info is not None and "nickname" in uid_info.keys():
            user = uid_info["nickname"]
        else:
            user = userinfo["user_id"]

    elif userinfo["message_type"] == "group":
        group_list = local_group_cache  # use cache first

        container = None
        for group_obj in group_list:
            if str(group_obj["group_id"]) == str(userinfo["group_id"]):
                container = group_obj["group_name"]
                break

        if container is None:
            # failed to get cache work. re-query.
            container = None
            try:
                group_list = bot.get_group_list()
            except Error as err:
                print("CQHTTP: 名称解析: 错误: 获取群列表失败(无缓存查询): 状态码=" + str((err.status_code, err.ret_code)))
                return None

            # update local group cache.
            local_group_cache = group_list

            for group_obj in group_list:
                if str(group_obj["group_id"]) == str(userinfo["group_id"]):
                    container = group_obj["group_name"]
                    break
            if container is None:
                # seems we can't query a result here.
                print("CQHTTP: 名称解析: 错误: 无法查询到请求的群组信息: GID=" + userinfo["group_id"])
                return None

        if partial:
            user = None
        else:
            if userinfo["sub_type"] == "normal":
                user_profile = None
                try:
                    user_profile = bot.get_group_member_info(group_id=userinfo["group_id"], user_id=userinfo["user_id"], no_cache=False)
                except Error as err:
                    print("CQHTTP: 名称解析: 警告: 缓存查询失败，正尝试无缓存查询: UID=" + userinfo["user_id"] + ", GID=" + userinfo["group_id"])
                    try:
                        user_profile = bot.get_group_member_info(group_id=userinfo["group_id"],
                                                                 user_id=userinfo["user_id"], no_cache=True)
                    except Error as err:
                        print("CQHTTP: 名称解析: 错误: 查询用户信息失败:  UID=" + userinfo["user_id"] + ", GID=" + userinfo["group_id"] + ", 状态码=" + str((err.status_code, err.ret_code)))

                if user_profile is None:
                    # seems something wrong. we return raw value back.
                    user = userinfo["user_id"]
                else:
                    # if card is present, use card.
                    if "card" in user_profile.keys() and not len(user_profile["card"]) == 0:
                        user = user_profile["card"]
                    else:
                        user = user_profile["nickname"]

            elif userinfo["sub_type"] == "anonymous":
                user = "[匿名] " + userinfo["anonymous"]

            elif userinfo["sub_type"] == "notice":
                user = "[系统通知]"
    elif userinfo["message_type"] == "discuss":
        # we don't have things to query.
        container = "讨论组=" + userinfo["discuss_id"]
        if partial:
            user = None
        else:
            # we have a user.
            try:
                # print("get user info: cache")
                uid_info = bot.get_stranger_info(user_id=userinfo["user_id"], no_cache=False)
            except Error:
                try:
                    print("CQHTTP: 名称解析: 警告: 缓存查询失败，正尝试无缓存查询: UID=" + userinfo["user_id"])
                    uid_info = bot.get_stranger_info(user_id=userinfo["user_id"], no_cache=True)  # refresh cache
                except Error as err:
                    print("CQHTTP: 名称解析: 错误: 查询用户信息失败:  UID=" + userinfo["user_id"] + ", 状态码=" + str(
                        (err.status_code, err.ret_code)))
                    uid_info = None
            if uid_info is not None and "nickname" in uid_info.keys():
                user = uid_info["nickname"]
            else:
                user = userinfo["user_id"]

    return platform, container, user
    pass

# except Error as err:
# # bot.get_group_list()
# print("CQHTTP: 名称解析: 错误: 查询群组列表失败: 状态码=" + str((err.status_code, err.ret_code)))
# return None


def _botstart(host, port):
    bot.run(host=host, port=port)


def _group_cache_worker():
    global cache_worker_enabled
    global local_group_cache
    counter = -1
    counter_max = 90
    while cache_worker_enabled:
        counter += 1
        if counter % counter_max == 0:
            # do our job
            counter = 0
            try:
                local_group_cache = bot.get_group_list()
            except Error as err:
                print("CQHTTP: 名称解析: 错误: 获取群列表失败(缓存任务): 状态码=" + str((err.status_code, err.ret_code)))
        else:
            #just sleep here
            # import random
            # print(str(random.randint(0x0000, 0xFFFF)))
            sleep(2)

def Setup(_sysbus: EventBus):
    global sysbus
    global bot
    global broadcast_receiver
    global broadcast_sender
    global UMP
    global bot_worker
    global cache_worker
    global cache_worker_enabled
    global local_group_cache
    global remote_resolver_lib
    global r_resolve
    global cfgobject

    local_group_cache = []

    sysbus = _sysbus
    print("CQHTTP: 初始化: 加载 UMP 对象")
    UMP = APIUMPModuleUtils(sysbus, MODULE_NAME, "CQHTTP")
    broadcast_receiver, broadcast_sender = UMP.get_controls()

    print("CQHTTP: 初始化: 载入配置文件")
    # read config
    cfgobject = load_config("CQHTTP.PluginConnection")
    api_root = cfgobject["cqhttp_api_root"]
    access_token = cfgobject["cqhttp_access_token"]
    secret = cfgobject["cqhttp_secret"]
    sdk_listen_address = cfgobject["sdk_listen_address"]
    sdk_listen_port = cfgobject["sdk_listen_port"]

    print("CQHTTP: 初始化: 建立 CQHTTP 插件连接")
    bot = CQHttp(api_root=api_root, access_token=access_token, secret=secret)
    bot_worker = Thread(target=_botstart, name="CQHTTP LocalConnect Worker",kwargs={
        "host": sdk_listen_address,
        "port": sdk_listen_port
    })

    print("CQHTTP: 初始化: 注册 UMP 处理例程")
    bot.set_handler("message", incoming_receiver)
    broadcast_receiver.register("OutgoingSender", outgoing_sender,
                                metadata_kwarg_name="metadata")

    print("CQHTTP: 初始化: 连接 UMP 名称解析服务")
    remote_resolver_lib = sysbus.APIManager.load_library("NameResolver")
    r_resolve = remote_resolver_lib.spawn_caller_module(ignore_api_check=False)
    r_resolve.register_resolver("Frontend.CQHTTP", "CQHTTP", name_resolver, )

    bot_worker.start()
    print("CQHTTP: 初始化: 群组列表缓存更新任务")
    cache_worker_enabled = True
    cache_worker = Thread(target=_group_cache_worker, name="CQHTTP Group List Cache Worker")
    cache_worker.start()
    print("CQHTTP: 初始化: 已完成")

def Takedown(sysbus):
    global broadcast_sender
    global broadcast_receiver
    global cache_worker_enabled
    global cache_worker
    print("CQHTTP: 结束任务: 群组列表缓存更新任务")
    cache_worker_enabled = False
    cache_worker.join()
    print("CQHTTP: 结束任务: 反注册 UMP 处理例程")
    bot.unset_handler("message")
    print("CQHTTP: 结束任务: 断开 CQHTTP 插件连接")
    bot.sdk_local_stop()
    print("CQHTTP: 结束任务: 已完成 ")
    pass

def ModuleInfo():
    return {
        "name": MODULE_NAME,
        "description": "酷Q HTTP API 接入适配器",
        "version": "0.1",
        "deprecated": "0.0",
        "require": [
            ("LiveComFW", "0.1"),
            ("Services.MessageRouter", "0.1"),
            ("Services.NameResolver", "0.1")
        ]
    }






